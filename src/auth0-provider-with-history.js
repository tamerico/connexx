import React from "react";
import { useHistory } from "react-router-dom";
import { Auth0Provider } from "@auth0/auth0-react";

const Auth0ProviderWithHistory = ({ children }) => {
  const history = useHistory();
  const domain = process.env.REACT_APP_AUTH0_DOMAIN;
  const clientId = process.env.REACT_APP_AUTH0_CLIENT_ID;
  const audience = process.env.REACT_APP_AUDIENCE; //novo para api express teste

  const oi = process.env.REACT_APP_TAMER; //novo para api express teste
  const just_oi = process.env.JUST_OI; //novo para api express teste
  
  console.log(domain);

  console.log(oi);
  console.log(just_oi);

  const onRedirectCallback = (appState) => {
    history.push(appState?.returnTo || window.location.pathname);
  };

  console.log("window.location.origin:");
  console.log(window.location.origin);

  return (
    <Auth0Provider
      domain={domain}
      clientId={clientId}
      redirectUri={window.location.origin}
      onRedirectCallback={onRedirectCallback}
      audience = {audience}
    >
      {children}
    </Auth0Provider>
  );
};

export default Auth0ProviderWithHistory;