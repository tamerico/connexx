// liberte-se do passado agora!
// 1.56 vamos lá ,seja o seu próprio herói. vença a si mesmo.

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { BrowserRouter as Router } from "react-router-dom";
import Auth0ProviderWithHistory from "./auth0-provider-with-history"; 

import './index.css';

// antigo formato import { BrowserRouter } from 'react-router-dom'

ReactDOM.render(
  <Router>
    <Auth0ProviderWithHistory>
          <App />

    </Auth0ProviderWithHistory>
  </Router>,
  document.getElementById("root")
);


// versao antiga sem auth0
// ReactDOM.render((
//   <BrowserRouter>
//     <App />
//   </BrowserRouter>
// ), document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
