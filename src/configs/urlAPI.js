
// All internal API call here
// TODO fazer a troca do endereço de developer para produção

var url = process.env.REACT_APP_API_PYTHON_CONNEXXME_URL; //novo para api express teste

console.log(url);
if (url=="http://localhost:9001")
{
  console.log("Ambiente dev. Ocultando endpoint nginx /candidate... ")
  url = "http://localhost:9001";
}
else
  url = url + "/candidate";  // endpoint para nginx redirecionar para o servico candidate



// quando estiver em produção a porta sera excluida pois sera associada ao diretorio 
// ex:  nginx recebe connexx.me/avatar  e direciona para o serviço 9001 (user)
const url_internal_api = 
{
  avatar_img : url +  '/avatar',
  introduction_values: url +  '/introduction',
  personal_values: url + '/personalvalues',
  benefits: url + '/benefits',
  citizenship: url + '/citizenship',
  feeds: url + '/feeds',
  languageslevels: url + '/languageslevels', 
  languages: url + '/languages',
  relocation: url + '/relocation',
  resume: url + '/resume'

};



export  default  url_internal_api ;

