/* pagina principal após o login */
/* palete de cores https://materializecss.com/color.html* */


// TODO implementar auth0 ou passportjs ou outro OIDC
// pirigando ser o auth0 por encontrar documentos https://auth0.com/blog/react-router-4-practical-tutorial/
// TODO implementar rotas privadas

import React from "react";
// Importando o component Home
import Home from "./components/home/home";
import Contact from "./components/contact/contact";
import Privacy from "./components/privacy/privacy";
import AccountSettings from "./components/accountSettings/accountSettings";
import FeedsProfile  from "./components/feeds/feeds";
import PrivateRoute from "./components/private-route";


// Importando os components necessários da lib react-materialize
import { Container, Footer } from 'react-materialize';

// https://stackoverflow.com/questions/42857283/react-router-typeerror-this-props-history-is-undefined
import { Switch, Route } from 'react-router-dom'


const Main = () => (
  <main className=" white  lighten-4">
    <Container>
    {/* <BrowserRouter basename={'/'}> */}
        <Switch>
            <PrivateRoute exact path='/' component={Home}/>
            <PrivateRoute path='/accountSettings' component={AccountSettings}/>
            <Route path='/contact' component={Contact}/>
            <Route path='/privacy' component={Privacy}/>
            <PrivateRoute path='/feeds' component={FeedsProfile}/>
            <PrivateRoute path='/logout' /> 
        </Switch>
        {/* </BrowserRouter> */}
          <Footer
                className="  light-blue lighten-5"
                copyrights="&copy 2015 Copyright Text"
                links={<ul><li><a className="grey-text text-lighten-3" href="#!">Link 1</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 2</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 3</a></li><li><a className="grey-text text-lighten-3" href="#!">Link 4</a></li></ul>}
                moreLinks={<a className="grey-text text-lighten-4 right" href="#!">More Links</a>}
                >
                <h5 className="white-text">
                    Footer Content
                </h5>
                <p className="black-text text-darken-4">
                    You can use rows and columns here to organize your footer content.
                </p>
          </Footer>
    </Container>

  </main>  
);

export default Main;