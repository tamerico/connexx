// Importando o React
import React from "react";
// Importando os components necessários da lib react-materialize
import { Navbar,   Icon} from 'react-materialize';

import { NavLink  } from 'react-router-dom'
import connexx_logo from '../../images/Logo.png';


const Header = () => (
  
    // <Navbar className=" teal  lighten-4">
    //   <li><NavLink to="/">Home</NavLink></li>
    //   <li><NavLink to="contact">Contact</NavLink></li>
    // </Navbar>



    <Navbar
      className= "white black-text"
      alignLinks="left"
      brand={<a className="brand-logo right" href="#"><img src={connexx_logo} style={{height: 150}} /></a>}
      id="mobile-nav"
      menuIcon={<Icon className="light-blue lighten-3 black-text">menu</Icon>}
      options={{
        draggable: true,
        edge: 'left',
        inDuration: 70,
        onCloseEnd: null,
        onCloseStart: null,
        onOpenEnd: null,
        onOpenStart: null,
        outDuration: 70,
        preventScrolling: true
      }}
    >
     <NavLink
      className= "light-blue lighten-3 black-text"
       to="/">Home</NavLink>
     <NavLink
       className= "light-blue lighten-3 black-text"
       to="accountSettings">Settings</NavLink>
       <NavLink
       className= "light-blue lighten-3 black-text"
       to="contact">Contact</NavLink>

        <NavLink
       className= "light-blue lighten-3 black-text"
       to="privacy">Privacy</NavLink>

       <NavLink
         className= "light-blue lighten-3 black-text"
       to="logout">Logout</NavLink>
    </Navbar>
  
);

export default Header;