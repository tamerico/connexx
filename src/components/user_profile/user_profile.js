
import React from "react";

import { Row, Col, Card } from 'react-materialize';
// importando a foto do usuário (note que depois podemos usa-la facilmente chamando apenas avatar ao invés do caminho completo)
import avatar from '../../images/avatar.jpeg';

const UserProfile = () => (
  <Card>
      <Row>
        <Col s={8} m={8} offset="s2 m2">
          <img src={avatar} alt="imgAvatar" className="circle responsive-img" />
        </Col>
      </Row>
      <Row className="center-align">
        <h5 >Tamer Americo</h5>
        <p className="grey darken-6 white-text">FullStack Developer</p>
        <p className="grey darken-4 white-text">Brazilian</p>
        <p className="grey darken-2 white-text">Microsoft</p>
      </Row>
  </Card>
);

export default UserProfile;