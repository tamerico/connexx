//https://www.digitalocean.com/community/tutorials/react-react-autocomplete
// este componente permite comunicacao pai e filho e vice versa.

import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import './autocomplete.css'

class Autocomplete extends Component {
  
  
  
  static propTypes = {  // propTypes faz  validação de tipos em tempo de desenvolvimento. nao aparece em producao.
    suggestions: PropTypes.instanceOf(Array),
    ownSuggestion: PropTypes.instanceOf(Boolean) //tamer    
  };

  static defaultProps = {
    suggestions: [],
    ownSuggestion: false //tamer
  };

  constructor(props) {
    super(props);

    this.state = {
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: "",
      inputPlaceHolder: ""
      

    };
  }

  // cirado por tamer Este método somente é chamado pelo componentDidMount associado ao REF no Parent
  // thanks , superhero : https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/ 
  updateInputValue = (val, val2) => {
    this.setState({
      userInput: val,
      inputPlaceHolder: val2
    });
  };


  // Event fired when the input value is changed
  onChange = e => {
    const { suggestions } = this.props;
    const userInput = e.currentTarget.value;

    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = suggestions.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    // Update the user input and filtered suggestions, reset the active
    // suggestion and make sure the suggestions are shown
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });
  };

  // Event fired when the user clicks on a suggestion
  onClick = e => {
    // Update the user input and reset the rest of the state
    this.props.tamer(e.currentTarget.innerText);//tamer . esta NOVA linha permite que o componente pai pegue o valor do innerText. gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });
  };


    // TODO pegar o texto que foi digitado
  onClickYourOwn = e => {
    // console.log(this.state.userInput);  nesta caso : this.state.userInput igual a e.currentTarget.innerText
    console.log(e.currentTarget.innerText);
    // Update the user input and reset the rest of the state
    this.props.tamer(e.currentTarget.innerText);//tamer . esta NOVA linha permite que o componente pai pegue o valor do innerText. gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText //this.state.userInput
    });
  };




  // Event fired when the user presses a key down
  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key, update the input and close the
    // suggestions
    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]
      });
    }
    // User pressed the up arrow, decrement the index
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow, increment the index
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  render() {
    
    

    
    const {
      onChange,
      onClick,
      onClickYourOwn, // tamer criativo
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput,
        inputPlaceHolder // tamer
      }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions"> 
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li
                  className={className}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions, you're on your own!</em> <button value={activeSuggestion} onClick={onClickYourOwn}>{this.state.userInput}</button>
          </div>
        );
      }
    }

    return (
      <Fragment>
        <label> {inputPlaceHolder}
          <input
            // style={{width: "90%"}}
            placeholder = {inputPlaceHolder}
            type="text"
            onChange={onChange}
            onKeyDown={onKeyDown}
            value={userInput}
          />
        </label>
        {suggestionsListComponent}
      </Fragment>
    );
  }
}

export default Autocomplete;