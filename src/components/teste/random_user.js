// Aqui é um teste para coletar dados de api.

import React, { Component } from "react";
import { Row, Col, Card } from 'react-materialize';

const API = 'https://randomuser.me/api/';


class RandomUser extends Component{


  // async componentDidMount() {
  //   await fetch(API )
  //     .then(response => response.json())
  //     .then(data => this.setState({ hits: data.hits }));
  // }

  state = {
    loading:true,
    person: null


  }

   async componentDidMount() {
     const response = await  fetch(API);
     const data =  await response.json();
     console.log(data);
     this.setState({person: data.results[0] , loading:false});
    // console.log(data.results[0]);
  }


  render() {
    return (
      
      <Row>
        <Col m={3} s={12}></Col>
        {this.state.loading || !this.state.person ?(
          <div>loading...</div>
        ):(
            <Card>
                <Row className="center-align">
                  <Col s={8} m={8} offset="s2 m2">
                    <img src={this.state.person.picture.large} alt="imgAvatarRandom" className="circle responsive-img" />
                  </Col>
                </Row>
                <Row className="center-align">
                 <h5 >{this.state.person.name.first}  {this.state.person.name.last}</h5>
                 <p className="grey darken-2 white-text"> {this.state.person.location.city}</p>
                  <p className="grey darken-6 white-text"> {this.state.person.email}</p>
                  <p className="grey darken-4 white-text"> {this.state.person.gender}</p>
                  
                </Row>
            </Card>
        )
        
        }

      </Row>
    );
  }


}
export default RandomUser;