import React from 'react';
import {Select } from 'react-materialize';


// http://react-materialize.github.io/react-materialize/?path=/story/components-radiogroup--default


/**
Low intermediate (B1)
High intermediate (B2)
Low advanced - close to native speaker (C1)
High advanced - native speaker (C2)
             * 
             */

        
            const LanguageLevel_options = () => (
              <Select
              id="Select-9"
              multiple={false}
              onChange={function noRefCheck(){}}
              options={{
                classes: '',
                dropdownOptions: {
                  alignment: 'left',
                  autoTrigger: true,
                  closeOnClick: true,
                  constrainWidth: true,
                  coverTrigger: true,
                  hover: false,
                  inDuration: 150,
                  onCloseEnd: null,
                  onCloseStart: null,
                  onOpenEnd: null,
                  onOpenStart: null,
                  outDuration: 250
                }
              }}
              value=""
              
            >
              <option
                disabled
                value=""
              >
                Choose your nationality
              </option>
              <option value="1">
                Option 1
              </option>
              <option value="2">
                Option 2
              </option>
              <option value="3">
                Option 3
              </option>
            </Select>
            );

            export default LanguageLevel_options;