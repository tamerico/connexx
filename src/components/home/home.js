
import React from "react";
// Importando os components necessários da lib react-materialize
import { Row, Col, Card } from 'react-materialize';

import UserProfile from '../user_profile/user_profile'
import Experience from '../user_experiences/user_experiences'
import Teste from  '../teste/teste.js'
import company_avatar from '../../images/company.jpeg';
// Importando o avatar da empresa 1
import company_avatar2 from '../../images/company2.jpeg';
import RandomUser from "../teste/random_user";

import LogoutButton from "../logout-button";

const Home = () => (
  <Row>
    <Col m={3} s={12}>
       <UserProfile /> 
        <RandomUser />
    </Col>
    <Col m={8} s={12}>
        <h5 className="subtitle">Dashboard</h5>
        <Card>
          <div>
          <LogoutButton></LogoutButton>
            <small>You are running this application in <b>{process.env.NODE_ENV}</b> mode.</small>
            <p><b>What we need to do?</b></p>
            <p>"submit your cv,  what is your personal values, we will find jobs."</p>
            <br/>
            <p><b>Where do you want to work?</b></p>
            <p>"Remotely or abroad?."</p>
            <br/>
            <p><b>Your progress, coaches, services</b></p>
            <p>"Go alone or go with help? Go beyond with curated information"</p>
          </div>
        </Card>
        <h5 className="subtitle">Offer Jobs matched your profile</h5>
        <Experience title="Developer"
                    company="Microsoft"
                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laborevoluptate velit esse cillum dolore eu fugiat id est laborum."
                    avatar={company_avatar}
        />
        <Experience title="React Developer"
                    company="Google"
                    description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut laborevoluptate velit esse cillum dolore eu fugiat id est laborum."
                    avatar={company_avatar2}                    
        /> 
        <div>oi: <Teste /></div>
        <div>random: <RandomUser /></div>
        
    </Col>
  </Row>
);

export default Home;