

function tenis(nome,telefone) {
  const element = (
    <div>
      <h1>{nome}</h1>
      <h2>{telefone}</h2>
    </div>
  );
  ReactDOM.render(element, document.getElementById('root'));
}

function tick() {
  const element = (
    <div>
      <h1>Hello, world!</h1>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  ReactDOM.render(element, document.getElementById('root'));
}



class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}


function getGreeting(user) {
  if (user) {
    return <h1>Hello,{user}!</h1>;
  }
  return <h1>Hello, Stranger.</h1>;
}

function breadCrumb (item){

  const breadcrumb = (
    <>
    { /* 
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== --> 
    */}
    <div class="row page-titles">
                        <div class="col-md-6 col-8 align-self-center">
                          <h3 class="text-themecolor m-b-0 m-t-0">{item}</h3>
                        </div>
     </div>          
</>  );

    ReactDOM.render(breadcrumb, document.getElementById('compBreadcrumb')  );

}



function inputText (nome, title, descr){

  const content = (
    <>
        <div class="form-group row">
  <label class="control-label text-right col-md-3">{title}</label>
        <div class="col-md-9">
            <input type="text" name="{nome}" placeholder="small" class="form-control"/>
  <small class="form-control-feedback"> {descr} </small> 
        </div>
        </div>
    </>
  );

  return content;


}


function formularioBasico (cidadao){
  
  

  const formulario = (
  <>
             
             <div class="row">
                  <div class="col-lg-12">
                      <div class="card card-outline-info">
                          <div class="card-header">
                              <h4 class="m-b-0 text-white">With Border at Bottom (<small>Use class form-bordered</small>)</h4>
                          </div>
                          <div class="card-block">
                              <form action="#" class="form-horizontal form-bordered">
                                  <div class="form-body">
                                     {inputText ("name", "Name", "type your name")}
                                     {inputText ("email", "E-mail", "your best e-mail")}
                                     {inputText ("password", "Password", "need change? ok")}
                                     {inputText ("passwordre", "Re-enter Password", "twice to match")}
                                     

                                  </div>
                                  <div class="form-actions">
                                      <div class="row">
                                          <div class="col-md-12">
                                              <div class="row">
                                                  <div class="offset-sm-3 col-md-9">
                                                      <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                      <button type="button" class="btn btn-inverse">Cancel</button>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
                       
              
              


  </>


);



  breadCrumb('Usuario > Configuracao');
  ReactDOM.render(formulario, document.getElementById('root'));

}


function formularioValores (item){ 
  const dashboard = (
    <>
            <div class="row">
                      <div class="col-lg-12">
                          <div class="card card-outline-info">
                              <div class="card-header">
                                  <h4 class="m-b-0 text-white">Lets configure your path (<small>we send data based on this answers</small>)</h4>
                              </div>
                              <div class="card-block">
                                  <form action="#" class="form-horizontal form-bordered">
                                      <div class="form-body">
                                          
                          
                                  
                                          <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Profetional Category</label>
                                              <div class="col-md-9">
                                                  <select class="form-control custom-select">
                                                      <option value="Category 1">tech</option>
                                                      <option value="Category 2">education</option>
                                                      <option value="Category 3">health</option>
                                                      <option value="Category 4">economics</option>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Personal Values</label>
                                              <div class="col-md-9">
                                                  <select class="form-control" multiple>
                                                    
                                                      
                                                          <option>ethics</option>
                                                          <option>multicultural</option>
                                                          <option>etc</option>
                                                          <option>etc</option>
                                                      
                                                  </select>
                                              </div>
                                          </div>
                                
                                        
                                        

                                      </div>
                                      <div class="form-actions">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <div class="row">
                                                      <div class="offset-sm-3 col-md-9">
                                                          <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Submit</button>
                                                          <button type="button" class="btn btn-inverse">Cancel</button>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>


                      <button onClick={formularioBasico}>
                          ir para formulario basico
                        </button>

                  </div>
            </>
  );

  breadCrumb('Usuario > Valores');
  ReactDOM.render(dashboard, document.getElementById('root'));

}




formularioValores();
//formularioBasico();



