import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import url_internal_api from '../../configs/urlAPI'
import {    Checkbox } from 'react-materialize';


// componente para atualizar imagem do candidato
//const API = 'http://developer:9001/avatar?email=tamer@tamerico.com';
const API = url_internal_api.feeds + '?email=tamer@tamerico.com';

class FeedsProfile extends Component {

  

  state = {
    loading:true,
    hits: []


  }

   async componentDidMount() {
     const response = await  fetch(API);
     const data =  await response.json();   // https://javascript.info/fetch
     console.log("response from: "+ API + ":");
     if (data.length>50)
      console.log(data.slice(0,50)+ "...  ");
      else
       console.log(data);
       console.log(data);
       console.log(Array.isArray(data));
     this.setState({hits: data , loading:false});
    // console.log(data.results[0]);
  }

  

  render(){

    
    const { hits } = this.state;
    console.log("hits original tamanho: "+(hits).length)

    return (
      <>
      {this.state.loading || !this.state.hits ?(
          <div>loading feeds Connexx.me...</div>
        ):(
        
        <div>
          
          {hits.map
            (hit =>
              <div className="row" key={hit.title} >
                <div className="col" > <img src={hit.img} /> </div>
                <div className="col" > {hit.title} {hit.link} <b> {hit.tags}</b> </div>
                 
                
              </div>
            )}
         
        
          </div>

         

        )}
      </>
    )


    




  }
 

} 
export default FeedsProfile;