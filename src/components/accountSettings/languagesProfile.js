
/**
 * Este componente manipula linguages e nivel de cada linguagem
 * 
 */

        // {/* https://github.com/forxer/languages-list/blob/master/src/Languages.csv */}
        // checkbox ou radio buttons para dizer o nivel de cada linguagem 


// lista de materiais http://react-materialize.github.io/react-materialize/?path=/story/components-textinput--default
import React, {Component} from 'react';
import {Icon, Col,  Button, TextInput} from  'react-materialize';
import Autocomplete from '../autocompleteComponent/autocompleteComponent'

import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'
const API = url_internal_api.languages;

export default class Languages extends Component {
  

  constructor(props){
    super(props);

    // tenho um pool de 10 campos para languages.
    this.state = {   // https://blog.stvmlbrn.com/2017/04/07/submitting-form-data-with-react.html
      language_1: '',
      language_2: '',
      language_3: '',
      language_4: '',
      language_5: '',
      language_6: '',
      language_7: '',
      language_8: '',
      language_9: '',
      language_10: '',
      loading:true
        }
  }


  


  // metodo especifico para o componente Autocomplete 
  prepareNewLanguageButton = (val) => {   //https://alligator.io/react/axios-react/
      console.log("add button language...:");
      console.log(val );
    
      // TODO  verifica o pool de botoes qual é o tamanho.
      // verifica o primeiro key/value vazio e adicionar o valor para ele
      const poolLanguages = this.state;
      delete poolLanguages['loading'];

      for (var poolX in poolLanguages) 
      {
        console.log("pool x= "+ poolX) 
        if ( poolX.startsWith("language_"))
          if  ( (poolLanguages[poolX]==null ) ||  (poolLanguages[poolX]=="" )  )
          {
            console.log (poolX + " esta livre");
            console.log (poolX + " atribuir valor :" + val);
            this.setState( {[poolX]: val});
            break;
          }
          else console.log (poolX + " esta ocupado :" + poolLanguages[poolX]);
      }
      console.log("fim do add buttons");
      
    }




  removeButtonLanguage = (e) => {   //https://alligator.io/react/axios-react/
      // https://reactjs.org/docs/handling-events.html   sobre synthetic events = e
      
      console.log("remove button lang:");
      console.log(e.target.id);
      this.setState({[e.target.id]:""});
    }


  // Gera o source para os botoes a partir do retorno dos dados do banco de dados
  generateButtons (){
      
      var LanguageButtonElements = {};
      
      const hits = this.state;

      console.log( ">>: " + (hits));
      console.log( (hits));

      console.log( typeof (hits));
      for (const propriedade in hits){
          if (propriedade.startsWith("language"))
          {
              console.log( '>>> ' + propriedade + ": " + hits[propriedade] );
              
              if ( hits[propriedade] != null)
                {
                  if ( hits[propriedade].length > 2)  // só pegar os que possuem conteudo
                {
                    // LanguageButtonElements.push(hits[propriedade]);
                  console.log( '### ' + propriedade + "= " + hits[propriedade] );
                  LanguageButtonElements[propriedade] = (hits[propriedade]);
                }
                else
                  delete LanguageButtonElements[propriedade];
                }
                else console.log("fucking null: "+propriedade );
          }


      }
      console.log( (LanguageButtonElements));

      return LanguageButtonElements;     
      

  }



  

  async componentDidMount() {

    //TODO  aqui fazer chamada api para pegar dados do banco
    const response = await fetch(API+'?email=tamer@tamerico.com');
    const data =  await response.json();   // https://javascript.info/fetch
    console.log("response from "+API + " language data  values: ");
    console.log(data);
    console.log("data languages values: ");
    // const languagesLevels = data;
    
    // const doBancoLang1 = "English B1";
    // const doBancoLang2 = "Portugues Native";
    var listaLanguages = {};
    for (var ips in data['languages'][0])  
          listaLanguages[ips] =   data['languages'][0][ips]
    listaLanguages['loading'] = false
    //  listaLanguages['language_01'] = "choose"
    //  listaLanguages['language_02'] = "choose2"
    
  // montar a parametrizacao dos estados baeado na quantidade de elementos vindas do banco
  // fazer um loop para adicionar N linguages no estado

    this.setState(   
      listaLanguages
      );


        

  }


// submit form
handleSubmit = event => {
  event.preventDefault();
  console.log("conteudo do formulario para languages:");
  console.log(event.target.elements); // exibe somente elementos do formulario

  console.log("conteudo do formulario. languages:");
  
  // montar parametros com linguagens
  var listaLanguages = {};
  var langFromState = this.state;
  delete langFromState['loading'];

    for (var currentLanguage in langFromState)
    {
      console.log ("processando para post: "+ currentLanguage) 

      if (currentLanguage.startsWith("language_"))
      {
        if (langFromState[currentLanguage] == null)
        {
          langFromState[currentLanguage] = "";
        }

        listaLanguages [ currentLanguage] =  langFromState[currentLanguage]
      }


    }  
   

  const newParamValues = 
    {
      "newLanguagesValues":  listaLanguages
    }


  axios.post(API, newParamValues)
    .then(res => {
      console.log("enviando POST languages _ values resposta: ");
      console.log(res);
      console.log(res.data);
    })
}




  render(){

    //const { language1, language2, visa_detail } = this.state;
    

    const  eleToRender = this.generateButtons();
    console.log(" tamanho do ele to render "+ Object.keys(eleToRender).length);
    console.log(typeof(eleToRender));
    console.log((eleToRender));
    var tagButtonElements = [];

    for (var botaoX in eleToRender)
    //for (var i=0; i<eleToRender.length; i++)
    { 
      console.log("eleToRender");
        console.log(eleToRender);
        
        tagButtonElements.push(
          // interessante observar que a definicao de uma funcao no onClick nao pode ter parenteses()
          // https://stackoverflow.com/questions/38401902/onclick-not-working-react-js
          <p key={botaoX}><Button  key={botaoX} id={botaoX} type="button" waves='light' 
                  onClick={(e) => this.removeButtonLanguage(e)} >{eleToRender[botaoX]}
            <Icon key={botaoX} right >
            {/* TODO adicionar evento ou stop propagagion ou similar para que o icone, ao ser clicado, execute a funcao do parent 
              avaliar https://stackoverflow.com/questions/38861601/how-to-only-trigger-parent-click-event-when-a-child-is-clicked */}
              close
            </Icon>
          </Button>
          </p>
          );
      }




      return (
      
          

          <form id="valuesformLanguageLevels" onSubmit={this.handleSubmit}>
                
               
             

            
              <Autocomplete
                tamer = {this.prepareNewLanguageButton}   //  update FROM child. isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
                suggestions={languagesLevels}
              />

                {tagButtonElements}
       
            <Col s={12} m={12}>
                    <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
            </Col>
      
          </form>


            
               
      );
}

}


// inserir isto como modal :  https://www.coe.int/en/web/common-european-framework-reference-languages

//https://github.com/forxer/languages-list/blob/master/src/Languages.csv

// conteudo gerado a partir do endpoint languagelevels em developer:9001/languageslevels. 
// conteudo foi copiado e colado aqui como array
const languagesLevels =  ["Abkhaz A1", "Abkhaz A2", "Abkhaz B1", "Abkhaz B2", "Abkhaz C1", "Abkhaz C2", "Abkhaz Native", "Afar A1", "Afar A2", "Afar B1", "Afar B2", "Afar C1", "Afar C2", "Afar Native", "Afrikaans A1", "Afrikaans A2", "Afrikaans B1", "Afrikaans B2", "Afrikaans C1", "Afrikaans C2", "Afrikaans Native", "Akan A1", "Akan A2", "Akan B1", "Akan B2", "Akan C1", "Akan C2", "Akan Native", "Albanian A1", "Albanian A2", "Albanian B1", "Albanian B2", "Albanian C1", "Albanian C2", "Albanian Native", "Amharic A1", "Amharic A2", "Amharic B1", "Amharic B2", "Amharic C1", "Amharic C2", "Amharic Native", "Arabic A1", "Arabic A2", "Arabic B1", "Arabic B2", "Arabic C1", "Arabic C2", "Arabic Native", "Aragonese A1", "Aragonese A2", "Aragonese B1", "Aragonese B2", "Aragonese C1", "Aragonese C2", "Aragonese Native", "Armenian A1", "Armenian A2", "Armenian B1", "Armenian B2", "Armenian C1", "Armenian C2", "Armenian Native", "Assamese A1", "Assamese A2", "Assamese B1", "Assamese B2", "Assamese C1", "Assamese C2", "Assamese Native", "Avaric A1", "Avaric A2", "Avaric B1", "Avaric B2", "Avaric C1", "Avaric C2", "Avaric Native", "Avestan A1", "Avestan A2", "Avestan B1", "Avestan B2", "Avestan C1", "Avestan C2", "Avestan Native", "Aymara A1", "Aymara A2", "Aymara B1", "Aymara B2", "Aymara C1", "Aymara C2", "Aymara Native", "Azerbaijani A1", "Azerbaijani A2", "Azerbaijani B1", "Azerbaijani B2", "Azerbaijani C1", "Azerbaijani C2", "Azerbaijani Native", "Bambara A1", "Bambara A2", "Bambara B1", "Bambara B2", "Bambara C1", "Bambara C2", "Bambara Native", "Bashkir A1", "Bashkir A2", "Bashkir B1", "Bashkir B2", "Bashkir C1", "Bashkir C2", "Bashkir Native", "Basque A1", "Basque A2", "Basque B1", "Basque B2", "Basque C1", "Basque C2", "Basque Native", "Belarusian A1", "Belarusian A2", "Belarusian B1", "Belarusian B2", "Belarusian C1", "Belarusian C2", "Belarusian Native", "Bengali A1", "Bengali A2", "Bengali B1", "Bengali B2", "Bengali C1", "Bengali C2", "Bengali Native", "Bihari A1", "Bihari A2", "Bihari B1", "Bihari B2", "Bihari C1", "Bihari C2", "Bihari Native", "Bislama A1", "Bislama A2", "Bislama B1", "Bislama B2", "Bislama C1", "Bislama C2", "Bislama Native", "Bosnian A1", "Bosnian A2", "Bosnian B1", "Bosnian B2", "Bosnian C1", "Bosnian C2", "Bosnian Native", "Breton A1", "Breton A2", "Breton B1", "Breton B2", "Breton C1", "Breton C2", "Breton Native", "Bulgarian A1", "Bulgarian A2", "Bulgarian B1", "Bulgarian B2", "Bulgarian C1", "Bulgarian C2", "Bulgarian Native", "Burmese A1", "Burmese A2", "Burmese B1", "Burmese B2", "Burmese C1", "Burmese C2", "Burmese Native", "Catalan; Valencian A1", "Catalan; Valencian A2", "Catalan; Valencian B1", "Catalan; Valencian B2", "Catalan; Valencian C1", "Catalan; Valencian C2", "Catalan; Valencian Native", "Chamorro A1", "Chamorro A2", "Chamorro B1", "Chamorro B2", "Chamorro C1", "Chamorro C2", "Chamorro Native", "Chechen A1", "Chechen A2", "Chechen B1", "Chechen B2", "Chechen C1", "Chechen C2", "Chechen Native", "Chichewa; Chewa; Nyanja A1", "Chichewa; Chewa; Nyanja A2", "Chichewa; Chewa; Nyanja B1", "Chichewa; Chewa; Nyanja B2", "Chichewa; Chewa; Nyanja C1", "Chichewa; Chewa; Nyanja C2", "Chichewa; Chewa; Nyanja Native", "Chinese A1", "Chinese A2", "Chinese B1", "Chinese B2", "Chinese C1", "Chinese C2", "Chinese Native", "Chuvash A1", "Chuvash A2", "Chuvash B1", "Chuvash B2", "Chuvash C1", "Chuvash C2", "Chuvash Native", "Cornish A1", "Cornish A2", "Cornish B1", "Cornish B2", "Cornish C1", "Cornish C2", "Cornish Native", "Corsican A1", "Corsican A2", "Corsican B1", "Corsican B2", "Corsican C1", "Corsican C2", "Corsican Native", "Cree A1", "Cree A2", "Cree B1", "Cree B2", "Cree C1", "Cree C2", "Cree Native", "Croatian A1", "Croatian A2", "Croatian B1", "Croatian B2", "Croatian C1", "Croatian C2", "Croatian Native", "Czech A1", "Czech A2", "Czech B1", "Czech B2", "Czech C1", "Czech C2", "Czech Native", "Danish A1", "Danish A2", "Danish B1", "Danish B2", "Danish C1", "Danish C2", "Danish Native", "Divehi; Dhivehi; Maldivian; A1", "Divehi; Dhivehi; Maldivian; A2", "Divehi; Dhivehi; Maldivian; B1", "Divehi; Dhivehi; Maldivian; B2", "Divehi; Dhivehi; Maldivian; C1", "Divehi; Dhivehi; Maldivian; C2", "Divehi; Dhivehi; Maldivian; Native", "Dutch A1", "Dutch A2", "Dutch B1", "Dutch B2", "Dutch C1", "Dutch C2", "Dutch Native", "English A1", "English A2", "English B1", "English B2", "English C1", "English C2", "English Native", "Esperanto A1", "Esperanto A2", "Esperanto B1", "Esperanto B2", "Esperanto C1", "Esperanto C2", "Esperanto Native", "Estonian A1", "Estonian A2", "Estonian B1", "Estonian B2", "Estonian C1", "Estonian C2", "Estonian Native", "Ewe A1", "Ewe A2", "Ewe B1", "Ewe B2", "Ewe C1", "Ewe C2", "Ewe Native", "Faroese A1", "Faroese A2", "Faroese B1", "Faroese B2", "Faroese C1", "Faroese C2", "Faroese Native", "Fijian A1", "Fijian A2", "Fijian B1", "Fijian B2", "Fijian C1", "Fijian C2", "Fijian Native", "Finnish A1", "Finnish A2", "Finnish B1", "Finnish B2", "Finnish C1", "Finnish C2", "Finnish Native", "French A1", "French A2", "French B1", "French B2", "French C1", "French C2", "French Native", "Fula; Fulah; Pulaar; Pular A1", "Fula; Fulah; Pulaar; Pular A2", "Fula; Fulah; Pulaar; Pular B1", "Fula; Fulah; Pulaar; Pular B2", "Fula; Fulah; Pulaar; Pular C1", "Fula; Fulah; Pulaar; Pular C2", "Fula; Fulah; Pulaar; Pular Native", "Galician A1", "Galician A2", "Galician B1", "Galician B2", "Galician C1", "Galician C2", "Galician Native", "Georgian A1", "Georgian A2", "Georgian B1", "Georgian B2", "Georgian C1", "Georgian C2", "Georgian Native", "German A1", "German A2", "German B1", "German B2", "German C1", "German C2", "German Native", "Greek, Modern A1", "Greek, Modern A2", "Greek, Modern B1", "Greek, Modern B2", "Greek, Modern C1", "Greek, Modern C2", "Greek, Modern Native", "Guaran\u00ed A1", "Guaran\u00ed A2", "Guaran\u00ed B1", "Guaran\u00ed B2", "Guaran\u00ed C1", "Guaran\u00ed C2", "Guaran\u00ed Native", "Gujarati A1", "Gujarati A2", "Gujarati B1", "Gujarati B2", "Gujarati C1", "Gujarati C2", "Gujarati Native", "Haitian; Haitian Creole A1", "Haitian; Haitian Creole A2", "Haitian; Haitian Creole B1", "Haitian; Haitian Creole B2", "Haitian; Haitian Creole C1", "Haitian; Haitian Creole C2", "Haitian; Haitian Creole Native", "Hausa A1", "Hausa A2", "Hausa B1", "Hausa B2", "Hausa C1", "Hausa C2", "Hausa Native", "Hebrew (modern) A1", "Hebrew (modern) A2", "Hebrew (modern) B1", "Hebrew (modern) B2", "Hebrew (modern) C1", "Hebrew (modern) C2", "Hebrew (modern) Native", "Herero A1", "Herero A2", "Herero B1", "Herero B2", "Herero C1", "Herero C2", "Herero Native", "Hindi A1", "Hindi A2", "Hindi B1", "Hindi B2", "Hindi C1", "Hindi C2", "Hindi Native", "Hiri Motu A1", "Hiri Motu A2", "Hiri Motu B1", "Hiri Motu B2", "Hiri Motu C1", "Hiri Motu C2", "Hiri Motu Native", "Hungarian A1", "Hungarian A2", "Hungarian B1", "Hungarian B2", "Hungarian C1", "Hungarian C2", "Hungarian Native", "Interlingua A1", "Interlingua A2", "Interlingua B1", "Interlingua B2", "Interlingua C1", "Interlingua C2", "Interlingua Native", "Indonesian A1", "Indonesian A2", "Indonesian B1", "Indonesian B2", "Indonesian C1", "Indonesian C2", "Indonesian Native", "Interlingue A1", "Interlingue A2", "Interlingue B1", "Interlingue B2", "Interlingue C1", "Interlingue C2", "Interlingue Native", "Irish A1", "Irish A2", "Irish B1", "Irish B2", "Irish C1", "Irish C2", "Irish Native", "Igbo A1", "Igbo A2", "Igbo B1", "Igbo B2", "Igbo C1", "Igbo C2", "Igbo Native", "Inupiaq A1", "Inupiaq A2", "Inupiaq B1", "Inupiaq B2", "Inupiaq C1", "Inupiaq C2", "Inupiaq Native", "Ido A1", "Ido A2", "Ido B1", "Ido B2", "Ido C1", "Ido C2", "Ido Native", "Icelandic A1", "Icelandic A2", "Icelandic B1", "Icelandic B2", "Icelandic C1", "Icelandic C2", "Icelandic Native", "Italian A1", "Italian A2", "Italian B1", "Italian B2", "Italian C1", "Italian C2", "Italian Native", "Inuktitut A1", "Inuktitut A2", "Inuktitut B1", "Inuktitut B2", "Inuktitut C1", "Inuktitut C2", "Inuktitut Native", "Japanese A1", "Japanese A2", "Japanese B1", "Japanese B2", "Japanese C1", "Japanese C2", "Japanese Native", "Javanese A1", "Javanese A2", "Javanese B1", "Javanese B2", "Javanese C1", "Javanese C2", "Javanese Native", "Kalaallisut, Greenlandic A1", "Kalaallisut, Greenlandic A2", "Kalaallisut, Greenlandic B1", "Kalaallisut, Greenlandic B2", "Kalaallisut, Greenlandic C1", "Kalaallisut, Greenlandic C2", "Kalaallisut, Greenlandic Native", "Kannada A1", "Kannada A2", "Kannada B1", "Kannada B2", "Kannada C1", "Kannada C2", "Kannada Native", "Kanuri A1", "Kanuri A2", "Kanuri B1", "Kanuri B2", "Kanuri C1", "Kanuri C2", "Kanuri Native", "Kashmiri A1", "Kashmiri A2", "Kashmiri B1", "Kashmiri B2", "Kashmiri C1", "Kashmiri C2", "Kashmiri Native", "Kazakh A1", "Kazakh A2", "Kazakh B1", "Kazakh B2", "Kazakh C1", "Kazakh C2", "Kazakh Native", "Khmer A1", "Khmer A2", "Khmer B1", "Khmer B2", "Khmer C1", "Khmer C2", "Khmer Native", "Kikuyu, Gikuyu A1", "Kikuyu, Gikuyu A2", "Kikuyu, Gikuyu B1", "Kikuyu, Gikuyu B2", "Kikuyu, Gikuyu C1", "Kikuyu, Gikuyu C2", "Kikuyu, Gikuyu Native", "Kinyarwanda A1", "Kinyarwanda A2", "Kinyarwanda B1", "Kinyarwanda B2", "Kinyarwanda C1", "Kinyarwanda C2", "Kinyarwanda Native", "Kirghiz, Kyrgyz A1", "Kirghiz, Kyrgyz A2", "Kirghiz, Kyrgyz B1", "Kirghiz, Kyrgyz B2", "Kirghiz, Kyrgyz C1", "Kirghiz, Kyrgyz C2", "Kirghiz, Kyrgyz Native", "Komi A1", "Komi A2", "Komi B1", "Komi B2", "Komi C1", "Komi C2", "Komi Native", "Kongo A1", "Kongo A2", "Kongo B1", "Kongo B2", "Kongo C1", "Kongo C2", "Kongo Native", "Korean A1", "Korean A2", "Korean B1", "Korean B2", "Korean C1", "Korean C2", "Korean Native", "Kurdish A1", "Kurdish A2", "Kurdish B1", "Kurdish B2", "Kurdish C1", "Kurdish C2", "Kurdish Native", "Kwanyama, Kuanyama A1", "Kwanyama, Kuanyama A2", "Kwanyama, Kuanyama B1", "Kwanyama, Kuanyama B2", "Kwanyama, Kuanyama C1", "Kwanyama, Kuanyama C2", "Kwanyama, Kuanyama Native", "Latin A1", "Latin A2", "Latin B1", "Latin B2", "Latin C1", "Latin C2", "Latin Native", "Luxembourgish, Letzeburgesch A1", "Luxembourgish, Letzeburgesch A2", "Luxembourgish, Letzeburgesch B1", "Luxembourgish, Letzeburgesch B2", "Luxembourgish, Letzeburgesch C1", "Luxembourgish, Letzeburgesch C2", "Luxembourgish, Letzeburgesch Native", "Luganda A1", "Luganda A2", "Luganda B1", "Luganda B2", "Luganda C1", "Luganda C2", "Luganda Native", "Limburgish, Limburgan, Limburger A1", "Limburgish, Limburgan, Limburger A2", "Limburgish, Limburgan, Limburger B1", "Limburgish, Limburgan, Limburger B2", "Limburgish, Limburgan, Limburger C1", "Limburgish, Limburgan, Limburger C2", "Limburgish, Limburgan, Limburger Native", "Lingala A1", "Lingala A2", "Lingala B1", "Lingala B2", "Lingala C1", "Lingala C2", "Lingala Native", "Lao A1", "Lao A2", "Lao B1", "Lao B2", "Lao C1", "Lao C2", "Lao Native", "Lithuanian A1", "Lithuanian A2", "Lithuanian B1", "Lithuanian B2", "Lithuanian C1", "Lithuanian C2", "Lithuanian Native", "Luba-Katanga A1", "Luba-Katanga A2", "Luba-Katanga B1", "Luba-Katanga B2", "Luba-Katanga C1", "Luba-Katanga C2", "Luba-Katanga Native", "Latvian A1", "Latvian A2", "Latvian B1", "Latvian B2", "Latvian C1", "Latvian C2", "Latvian Native", "Manx A1", "Manx A2", "Manx B1", "Manx B2", "Manx C1", "Manx C2", "Manx Native", "Macedonian A1", "Macedonian A2", "Macedonian B1", "Macedonian B2", "Macedonian C1", "Macedonian C2", "Macedonian Native", "Malagasy A1", "Malagasy A2", "Malagasy B1", "Malagasy B2", "Malagasy C1", "Malagasy C2", "Malagasy Native", "Malay A1", "Malay A2", "Malay B1", "Malay B2", "Malay C1", "Malay C2", "Malay Native", "Malayalam A1", "Malayalam A2", "Malayalam B1", "Malayalam B2", "Malayalam C1", "Malayalam C2", "Malayalam Native", "Maltese A1", "Maltese A2", "Maltese B1", "Maltese B2", "Maltese C1", "Maltese C2", "Maltese Native", "M\u0101ori A1", "M\u0101ori A2", "M\u0101ori B1", "M\u0101ori B2", "M\u0101ori C1", "M\u0101ori C2", "M\u0101ori Native", "Marathi (Mar\u0101\u1e6dh\u012b) A1", "Marathi (Mar\u0101\u1e6dh\u012b) A2", "Marathi (Mar\u0101\u1e6dh\u012b) B1", "Marathi (Mar\u0101\u1e6dh\u012b) B2", "Marathi (Mar\u0101\u1e6dh\u012b) C1", "Marathi (Mar\u0101\u1e6dh\u012b) C2", "Marathi (Mar\u0101\u1e6dh\u012b) Native", "Marshallese A1", "Marshallese A2", "Marshallese B1", "Marshallese B2", "Marshallese C1", "Marshallese C2", "Marshallese Native", "Mongolian A1", "Mongolian A2", "Mongolian B1", "Mongolian B2", "Mongolian C1", "Mongolian C2", "Mongolian Native", "Nauru A1", "Nauru A2", "Nauru B1", "Nauru B2", "Nauru C1", "Nauru C2", "Nauru Native", "Navajo, Navaho A1", "Navajo, Navaho A2", "Navajo, Navaho B1", "Navajo, Navaho B2", "Navajo, Navaho C1", "Navajo, Navaho C2", "Navajo, Navaho Native", "Norwegian Bokm\u00e5l A1", "Norwegian Bokm\u00e5l A2", "Norwegian Bokm\u00e5l B1", "Norwegian Bokm\u00e5l B2", "Norwegian Bokm\u00e5l C1", "Norwegian Bokm\u00e5l C2", "Norwegian Bokm\u00e5l Native", "North Ndebele A1", "North Ndebele A2", "North Ndebele B1", "North Ndebele B2", "North Ndebele C1", "North Ndebele C2", "North Ndebele Native", "Nepali A1", "Nepali A2", "Nepali B1", "Nepali B2", "Nepali C1", "Nepali C2", "Nepali Native", "Ndonga A1", "Ndonga A2", "Ndonga B1", "Ndonga B2", "Ndonga C1", "Ndonga C2", "Ndonga Native", "Norwegian Nynorsk A1", "Norwegian Nynorsk A2", "Norwegian Nynorsk B1", "Norwegian Nynorsk B2", "Norwegian Nynorsk C1", "Norwegian Nynorsk C2", "Norwegian Nynorsk Native", "Norwegian A1", "Norwegian A2", "Norwegian B1", "Norwegian B2", "Norwegian C1", "Norwegian C2", "Norwegian Native", "Nuosu A1", "Nuosu A2", "Nuosu B1", "Nuosu B2", "Nuosu C1", "Nuosu C2", "Nuosu Native", "South Ndebele A1", "South Ndebele A2", "South Ndebele B1", "South Ndebele B2", "South Ndebele C1", "South Ndebele C2", "South Ndebele Native", "Occitan A1", "Occitan A2", "Occitan B1", "Occitan B2", "Occitan C1", "Occitan C2", "Occitan Native", "Ojibwe, Ojibwa A1", "Ojibwe, Ojibwa A2", "Ojibwe, Ojibwa B1", "Ojibwe, Ojibwa B2", "Ojibwe, Ojibwa C1", "Ojibwe, Ojibwa C2", "Ojibwe, Ojibwa Native", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic A1", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic A2", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic B1", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic B2", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic C1", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic C2", "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic Native", "Oromo A1", "Oromo A2", "Oromo B1", "Oromo B2", "Oromo C1", "Oromo C2", "Oromo Native", "Oriya A1", "Oriya A2", "Oriya B1", "Oriya B2", "Oriya C1", "Oriya C2", "Oriya Native", "Ossetian, Ossetic A1", "Ossetian, Ossetic A2", "Ossetian, Ossetic B1", "Ossetian, Ossetic B2", "Ossetian, Ossetic C1", "Ossetian, Ossetic C2", "Ossetian, Ossetic Native", "Panjabi, Punjabi A1", "Panjabi, Punjabi A2", "Panjabi, Punjabi B1", "Panjabi, Punjabi B2", "Panjabi, Punjabi C1", "Panjabi, Punjabi C2", "Panjabi, Punjabi Native", "P\u0101li A1", "P\u0101li A2", "P\u0101li B1", "P\u0101li B2", "P\u0101li C1", "P\u0101li C2", "P\u0101li Native", "Persian A1", "Persian A2", "Persian B1", "Persian B2", "Persian C1", "Persian C2", "Persian Native", "Polish A1", "Polish A2", "Polish B1", "Polish B2", "Polish C1", "Polish C2", "Polish Native", "Pashto, Pushto A1", "Pashto, Pushto A2", "Pashto, Pushto B1", "Pashto, Pushto B2", "Pashto, Pushto C1", "Pashto, Pushto C2", "Pashto, Pushto Native", "Portuguese A1", "Portuguese A2", "Portuguese B1", "Portuguese B2", "Portuguese C1", "Portuguese C2", "Portuguese Native", "Quechua A1", "Quechua A2", "Quechua B1", "Quechua B2", "Quechua C1", "Quechua C2", "Quechua Native", "Romansh A1", "Romansh A2", "Romansh B1", "Romansh B2", "Romansh C1", "Romansh C2", "Romansh Native", "Kirundi A1", "Kirundi A2", "Kirundi B1", "Kirundi B2", "Kirundi C1", "Kirundi C2", "Kirundi Native", "Romanian, Moldavian, Moldovan A1", "Romanian, Moldavian, Moldovan A2", "Romanian, Moldavian, Moldovan B1", "Romanian, Moldavian, Moldovan B2", "Romanian, Moldavian, Moldovan C1", "Romanian, Moldavian, Moldovan C2", "Romanian, Moldavian, Moldovan Native", "Russian A1", "Russian A2", "Russian B1", "Russian B2", "Russian C1", "Russian C2", "Russian Native", "Sanskrit (Sa\u1e41sk\u1e5bta) A1", "Sanskrit (Sa\u1e41sk\u1e5bta) A2", "Sanskrit (Sa\u1e41sk\u1e5bta) B1", "Sanskrit (Sa\u1e41sk\u1e5bta) B2", "Sanskrit (Sa\u1e41sk\u1e5bta) C1", "Sanskrit (Sa\u1e41sk\u1e5bta) C2", "Sanskrit (Sa\u1e41sk\u1e5bta) Native", "Sardinian A1", "Sardinian A2", "Sardinian B1", "Sardinian B2", "Sardinian C1", "Sardinian C2", "Sardinian Native", "Sindhi A1", "Sindhi A2", "Sindhi B1", "Sindhi B2", "Sindhi C1", "Sindhi C2", "Sindhi Native", "Northern Sami A1", "Northern Sami A2", "Northern Sami B1", "Northern Sami B2", "Northern Sami C1", "Northern Sami C2", "Northern Sami Native", "Samoan A1", "Samoan A2", "Samoan B1", "Samoan B2", "Samoan C1", "Samoan C2", "Samoan Native", "Sango A1", "Sango A2", "Sango B1", "Sango B2", "Sango C1", "Sango C2", "Sango Native", "Serbian A1", "Serbian A2", "Serbian B1", "Serbian B2", "Serbian C1", "Serbian C2", "Serbian Native", "Scottish Gaelic; Gaelic A1", "Scottish Gaelic; Gaelic A2", "Scottish Gaelic; Gaelic B1", "Scottish Gaelic; Gaelic B2", "Scottish Gaelic; Gaelic C1", "Scottish Gaelic; Gaelic C2", "Scottish Gaelic; Gaelic Native", "Shona A1", "Shona A2", "Shona B1", "Shona B2", "Shona C1", "Shona C2", "Shona Native", "Sinhala, Sinhalese A1", "Sinhala, Sinhalese A2", "Sinhala, Sinhalese B1", "Sinhala, Sinhalese B2", "Sinhala, Sinhalese C1", "Sinhala, Sinhalese C2", "Sinhala, Sinhalese Native", "Slovak A1", "Slovak A2", "Slovak B1", "Slovak B2", "Slovak C1", "Slovak C2", "Slovak Native", "Slovene A1", "Slovene A2", "Slovene B1", "Slovene B2", "Slovene C1", "Slovene C2", "Slovene Native", "Somali A1", "Somali A2", "Somali B1", "Somali B2", "Somali C1", "Somali C2", "Somali Native", "Southern Sotho A1", "Southern Sotho A2", "Southern Sotho B1", "Southern Sotho B2", "Southern Sotho C1", "Southern Sotho C2", "Southern Sotho Native", "Spanish; Castilian A1", "Spanish; Castilian A2", "Spanish; Castilian B1", "Spanish; Castilian B2", "Spanish; Castilian C1", "Spanish; Castilian C2", "Spanish; Castilian Native", "Sundanese A1", "Sundanese A2", "Sundanese B1", "Sundanese B2", "Sundanese C1", "Sundanese C2", "Sundanese Native", "Swahili A1", "Swahili A2", "Swahili B1", "Swahili B2", "Swahili C1", "Swahili C2", "Swahili Native", "Swati A1", "Swati A2", "Swati B1", "Swati B2", "Swati C1", "Swati C2", "Swati Native", "Swedish A1", "Swedish A2", "Swedish B1", "Swedish B2", "Swedish C1", "Swedish C2", "Swedish Native", "Tamil A1", "Tamil A2", "Tamil B1", "Tamil B2", "Tamil C1", "Tamil C2", "Tamil Native", "Telugu A1", "Telugu A2", "Telugu B1", "Telugu B2", "Telugu C1", "Telugu C2", "Telugu Native", "Tajik A1", "Tajik A2", "Tajik B1", "Tajik B2", "Tajik C1", "Tajik C2", "Tajik Native", "Thai A1", "Thai A2", "Thai B1", "Thai B2", "Thai C1", "Thai C2", "Thai Native", "Tigrinya A1", "Tigrinya A2", "Tigrinya B1", "Tigrinya B2", "Tigrinya C1", "Tigrinya C2", "Tigrinya Native", "Tibetan Standard, Tibetan, Central A1", "Tibetan Standard, Tibetan, Central A2", "Tibetan Standard, Tibetan, Central B1", "Tibetan Standard, Tibetan, Central B2", "Tibetan Standard, Tibetan, Central C1", "Tibetan Standard, Tibetan, Central C2", "Tibetan Standard, Tibetan, Central Native", "Turkmen A1", "Turkmen A2", "Turkmen B1", "Turkmen B2", "Turkmen C1", "Turkmen C2", "Turkmen Native", "Tagalog A1", "Tagalog A2", "Tagalog B1", "Tagalog B2", "Tagalog C1", "Tagalog C2", "Tagalog Native", "Tswana A1", "Tswana A2", "Tswana B1", "Tswana B2", "Tswana C1", "Tswana C2", "Tswana Native", "Tonga (Tonga Islands) A1", "Tonga (Tonga Islands) A2", "Tonga (Tonga Islands) B1", "Tonga (Tonga Islands) B2", "Tonga (Tonga Islands) C1", "Tonga (Tonga Islands) C2", "Tonga (Tonga Islands) Native", "Turkish A1", "Turkish A2", "Turkish B1", "Turkish B2", "Turkish C1", "Turkish C2", "Turkish Native", "Tsonga A1", "Tsonga A2", "Tsonga B1", "Tsonga B2", "Tsonga C1", "Tsonga C2", "Tsonga Native", "Tatar A1", "Tatar A2", "Tatar B1", "Tatar B2", "Tatar C1", "Tatar C2", "Tatar Native", "Twi A1", "Twi A2", "Twi B1", "Twi B2", "Twi C1", "Twi C2", "Twi Native", "Tahitian A1", "Tahitian A2", "Tahitian B1", "Tahitian B2", "Tahitian C1", "Tahitian C2", "Tahitian Native", "Uighur, Uyghur A1", "Uighur, Uyghur A2", "Uighur, Uyghur B1", "Uighur, Uyghur B2", "Uighur, Uyghur C1", "Uighur, Uyghur C2", "Uighur, Uyghur Native", "Ukrainian A1", "Ukrainian A2", "Ukrainian B1", "Ukrainian B2", "Ukrainian C1", "Ukrainian C2", "Ukrainian Native", "Urdu A1", "Urdu A2", "Urdu B1", "Urdu B2", "Urdu C1", "Urdu C2", "Urdu Native", "Uzbek A1", "Uzbek A2", "Uzbek B1", "Uzbek B2", "Uzbek C1", "Uzbek C2", "Uzbek Native", "Venda A1", "Venda A2", "Venda B1", "Venda B2", "Venda C1", "Venda C2", "Venda Native", "Vietnamese A1", "Vietnamese A2", "Vietnamese B1", "Vietnamese B2", "Vietnamese C1", "Vietnamese C2", "Vietnamese Native", "Volap\u00fck A1", "Volap\u00fck A2", "Volap\u00fck B1", "Volap\u00fck B2", "Volap\u00fck C1", "Volap\u00fck C2", "Volap\u00fck Native", "Walloon A1", "Walloon A2", "Walloon B1", "Walloon B2", "Walloon C1", "Walloon C2", "Walloon Native", "Welsh A1", "Welsh A2", "Welsh B1", "Welsh B2", "Welsh C1", "Welsh C2", "Welsh Native", "Wolof A1", "Wolof A2", "Wolof B1", "Wolof B2", "Wolof C1", "Wolof C2", "Wolof Native", "Western Frisian A1", "Western Frisian A2", "Western Frisian B1", "Western Frisian B2", "Western Frisian C1", "Western Frisian C2", "Western Frisian Native", "Xhosa A1", "Xhosa A2", "Xhosa B1", "Xhosa B2", "Xhosa C1", "Xhosa C2", "Xhosa Native", "Yiddish A1", "Yiddish A2", "Yiddish B1", "Yiddish B2", "Yiddish C1", "Yiddish C2", "Yiddish Native", "Yoruba A1", "Yoruba A2", "Yoruba B1", "Yoruba B2", "Yoruba C1", "Yoruba C2", "Yoruba Native", "Zhuang, Chuang A1", "Zhuang, Chuang A2", "Zhuang, Chuang B1", "Zhuang, Chuang B2", "Zhuang, Chuang C1", "Zhuang, Chuang C2", "Zhuang, Chuang Native"]
