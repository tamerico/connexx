/* eslint-disable no-use-before-define */

/**
 * Este componente manipula cidadania e visto 
 * 
 */


// lista de materiais http://react-materialize.github.io/react-materialize/?path=/story/components-textinput--default
import React, {Component} from 'react';
import {Select, Col, Row,  Button, TextInput} from  'react-materialize';
import Autocomplete from '../autocompleteComponent/autocompleteComponent'  // personalizacacao do componente.
import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'



const API = url_internal_api.citizenship;



export default class Citizenship extends Component {
  

  constructor(props){
    super(props);


    // para atulalizar nat1 e nat2 no child
    this.citizenship1Element = React.createRef()// para enviar dados para o child https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    this.citizenship2Element = React.createRef()// para enviar dados para o child https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    // para atualizar visa_country no child    
    this.visa_countryElement = React.createRef()// para enviar dados para o child https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/    

    this.state = {   // https://blog.stvmlbrn.com/2017/04/07/submitting-form-data-with-react.html
      citizenship1: '',
      citizenship2: '',
      visa_type: '',
      visa_type_text: 'Choose your option',
      visa_country: '',
      loading:true
        }
  }


  

/**
   * este metodo armazena o estado do que for preenchido no formulario
   */
handleChangeVisa_type = event => {   //https://alligator.io/react/axios-react/
    console.log("visa handle change  value:")
    console.log(">>" + event.target.name + " <<" + event.target.value + "--");
    
   this.setState({  "visa_type": event.target.value });
    //this.setState({visa_type: event.target.value});
 }
 

// metodo especifico para o compoente Autocomplete 
 handleChangeCitizenship1 = (val) => {   //https://alligator.io/react/axios-react/
    console.log("handle change  value:")
    console.log(val );
    this.setState({  "citizenship1": val}); // update FROM child . gracias  https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
  

 }


// metodo especifico para o compoente Autocomplete 
 handleChangeCitizenship2 = (val) => {   //https://alligator.io/react/axios-react/
  console.log("handle change  value:")
  console.log(val );
 
  // define estado que é resgatado do componente filho
  this.setState({  "citizenship2": val}); // update FROM child.  gracias  https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
}



// metodo especifico para o compoente Autocomplete 
handleChangeVisaCountry = (val) => {   //https://alligator.io/react/axios-react/
  console.log("handle change  value:")
  console.log(val );
 
  // define estado que é resgatado do componente filho
  this.setState({  "visa_country": val}); // update FROM child.  gracias  https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
}




handleSubmit = event => {
  event.preventDefault();
  console.log("conteudo do formulario para citizenship:");
  console.log(event.target.elements); // coleta somente elementos do formulario

  console.log("conteudo do formulario. citizenship:");
  console.log(this.state.citizenship1);
  console.log(this.state.citizenship2);
  console.log(this.state.visa_type);
  console.log(this.state.visa_country);
      

  const newParamValues = 
    {
      "newCitizenshipValues": {
        "country1": this.state.citizenship1,
        "country2": this.state.citizenship2,
        "visa_type": this.state.visa_type,
        "visa_country": this.state.visa_country
        
      }
    }


  axios.post(API, newParamValues)
    .then(res => {
      console.log("enviando POST citizenship _ values resposta: ");
      console.log(res);
      console.log(res.data);
    })
}



 

async componentDidMount() {

  
  const response = await  fetch(API+'?email=tamer@tamerico.com');
  const data =  await response.json();   // https://javascript.info/fetch  :  response.json() – parse the response as JSON, nao sendo necessario usar o JSON.parse(conteudoString)
  console.log("response from "+API + " data  values: ");
  console.log(data);
  console.log("datacitizenshop: ");
  console.log(data.citizenship);

  

  let doBancoNat1 = data.citizenship[0]['country1']; //"Brazilian";
  let doBancoNat2 = data.citizenship[0]['country2']; //"Brazilian";
  let doBancoVisa_type = data.citizenship[0]['visa_type'];
  let doBancoVisa_type_default_text = "Choose your option";
  let doBancoVisa_country = data.citizenship[0]['visa_country']; // ex: Brazil


  if (doBancoNat1 == null)
  {
    doBancoNat1 ="";
    console.log("dobancocit null");
  }
    
  if (doBancoNat2==null)
  {
    doBancoNat2 ="";
    console.log("dobancocit2 null");
  }

  if 
  ( doBancoVisa_type==null ||   doBancoVisa_type.length==0   )
  {
    doBancoVisa_type ="No Visa";
    console.log("dobancovisa null");
  }
  else{
    doBancoVisa_type_default_text = doBancoVisa_type;
    console.log("doBancoVisa_type_default_text");
  } 
  
  if (doBancoVisa_country==null)
  {
    doBancoVisa_country ="";
    console.log("doBancoVisa_country null");
  }
  


  this.setState(   
     {
      citizenship1: doBancoNat1,
      citizenship2: doBancoNat2,
      visa_type: doBancoVisa_type,
      visa_type_text: doBancoVisa_type_default_text,
      visa_country: doBancoVisa_country,
      loading:false
      }
     );

     
    this.citizenship1Element.current.updateInputValue(doBancoNat1, "Citizenship"); // update from Parent TO child. gracias https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    this.citizenship2Element.current.updateInputValue(doBancoNat2, "other Citizenship"); // update from Parent TO child. gracias https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/

    this.visa_countryElement.current.updateInputValue(doBancoVisa_country, "Visa Country"); // update from Parent TO child. gracias https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
   

}



  render(){

    //const { citizenship1, citizenship2, visa_type, visa_type_text, visa_country } = this.state;

      return (
      <>
      

      <form id="valuesformCitizenship" onSubmit={this.handleSubmit}>
        <Row>
        <Col s={12} m={12}>
          <Autocomplete
            ref = {this.citizenship1Element} // iso é para atualizar o child apos pegar dados do DB
            tamer = {this.handleChangeCitizenship1}   // update FROM child.  isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
            suggestions={nationalities}
          />
          </Col>

          <Col s={12} m={12}>
          <Autocomplete
            ref = {this.citizenship2Element} // iso é para atualizar o child apos pegar dados do DB
            tamer = {this.handleChangeCitizenship2}   //  update FROM child. isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
            suggestions={nationalities}
          />
          </Col>

          <Col s={12} m={12}>
          <Select
                    label= "Visa status"
                    onChange = {this.handleChangeVisa_type}
                    id="visa_type"
                    multiple={false}
                    options={{
                      classes: '',
                      dropdownOptions: {
                        alignment: 'left',
                        autoTrigger: true,
                        closeOnClick: true,
                        constrainWidth: true,
                        coverTrigger: true,
                        hover: false,
                        inDuration: 150,
                        onCloseEnd: null,
                        onCloseStart: null,
                        onOpenEnd: null,
                        onOpenStart: null,
                        outDuration: 250
                      }
                    }}
                    value={this.state.visa_type}
                  >
                    <option
                      disabled
                      value=""
                    >
                     {this.state.visa_type_text} 
                    </option>
                    
                    <option value="No Visa">
                      No Visa
                    </option>

                    <option value="Student">
                      Student
                    </option>

                    <option value="Work Visa">
                      Work Visa
                    </option>

                    <option value="Permanent Resident">
                      Permanent Resident
                    </option>
            </Select>
           </Col>

           <Col s={12} m={12}>
            <Autocomplete
            ref = {this.visa_countryElement} // iso é para atualizar o child apos pegar dados do DB
            tamer = {this.handleChangeVisaCountry}   //  update FROM child. isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
            suggestions={countryList}
          />
          </Col>

        <Col s={12} m={12}>
                <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
        </Col>

        </Row>



        
       </form>


         
      </>              
  );
}

}


const countryList = [
	"Afghanistan",
	"Albania",
	"Algeria",
	"American Samoa",
	"Andorra",
	"Angola",
	"Anguilla",
	"Antarctica",
	"Antigua and Barbuda",
	"Argentina",
	"Armenia",
	"Aruba",
	"Australia",
	"Austria",
	"Azerbaijan",
	"Bahamas (the)",
	"Bahrain",
	"Bangladesh",
	"Barbados",
	"Belarus",
	"Belgium",
	"Belize",
	"Benin",
	"Bermuda",
	"Bhutan",
	"Bolivia (Plurinational State of)",
	"Bonaire, Sint Eustatius and Saba",
	"Bosnia and Herzegovina",
	"Botswana",
	"Bouvet Island",
	"Brazil",
	"British Indian Ocean Territory (the)",
	"Brunei Darussalam",
	"Bulgaria",
	"Burkina Faso",
	"Burundi",
	"Cabo Verde",
	"Cambodia",
	"Cameroon",
	"Canada",
	"Cayman Islands (the)",
	"Central African Republic (the)",
	"Chad",
	"Chile",
	"China",
	"Christmas Island",
	"Cocos (Keeling) Islands (the)",
	"Colombia",
	"Comoros (the)",
	"Congo (the Democratic Republic of the)",
	"Congo (the)",
	"Cook Islands (the)",
	"Costa Rica",
	"Croatia",
	"Cuba",
	"Curaçao",
	"Cyprus",
	"Czechia",
	"Côte d'Ivoire",
	"Denmark",
	"Djibouti",
	"Dominica",
	"Dominican Republic (the)",
	"Ecuador",
	"Egypt",
	"El Salvador",
	"Equatorial Guinea",
	"Eritrea",
	"Estonia",
	"Eswatini",
	"Ethiopia",
	"Falkland Islands (the) [Malvinas]",
	"Faroe Islands (the)",
	"Fiji",
	"Finland",
	"France",
	"French Guiana",
	"French Polynesia",
	"French Southern Territories (the)",
	"Gabon",
	"Gambia (the)",
	"Georgia",
	"Germany",
	"Ghana",
	"Gibraltar",
	"Greece",
	"Greenland",
	"Grenada",
	"Guadeloupe",
	"Guam",
	"Guatemala",
	"Guernsey",
	"Guinea",
	"Guinea-Bissau",
	"Guyana",
	"Haiti",
	"Heard Island and McDonald Islands",
	"Holy See (the)",
	"Honduras",
	"Hong Kong",
	"Hungary",
	"Iceland",
	"India",
	"Indonesia",
	"Iran (Islamic Republic of)",
	"Iraq",
	"Ireland",
	"Isle of Man",
	"Israel",
	"Italy",
	"Jamaica",
	"Japan",
	"Jersey",
	"Jordan",
	"Kazakhstan",
	"Kenya",
	"Kiribati",
	"Korea (the Democratic People's Republic of)",
	"Korea (the Republic of)",
	"Kuwait",
	"Kyrgyzstan",
	"Lao People's Democratic Republic (the)",
	"Latvia",
	"Lebanon",
	"Lesotho",
	"Liberia",
	"Libya",
	"Liechtenstein",
	"Lithuania",
	"Luxembourg",
	"Macao",
	"Madagascar",
	"Malawi",
	"Malaysia",
	"Maldives",
	"Mali",
	"Malta",
	"Marshall Islands (the)",
	"Martinique",
	"Mauritania",
	"Mauritius",
	"Mayotte",
	"Mexico",
	"Micronesia (Federated States of)",
	"Moldova (the Republic of)",
	"Monaco",
	"Mongolia",
	"Montenegro",
	"Montserrat",
	"Morocco",
	"Mozambique",
	"Myanmar",
	"Namibia",
	"Nauru",
	"Nepal",
	"Netherlands (the)",
	"New Caledonia",
	"New Zealand",
	"Nicaragua",
	"Niger (the)",
	"Nigeria",
	"Niue",
	"Norfolk Island",
	"Northern Mariana Islands (the)",
	"Norway",
	"Oman",
	"Pakistan",
	"Palau",
	"Palestine, State of",
	"Panama",
	"Papua New Guinea",
	"Paraguay",
	"Peru",
	"Philippines (the)",
	"Pitcairn",
	"Poland",
	"Portugal",
	"Puerto Rico",
	"Qatar",
	"Republic of North Macedonia",
	"Romania",
	"Russian Federation (the)",
	"Rwanda",
	"Réunion",
	"Saint Barthélemy",
	"Saint Helena, Ascension and Tristan da Cunha",
	"Saint Kitts and Nevis",
	"Saint Lucia",
	"Saint Martin (French part)",
	"Saint Pierre and Miquelon",
	"Saint Vincent and the Grenadines",
	"Samoa",
	"San Marino",
	"Sao Tome and Principe",
	"Saudi Arabia",
	"Senegal",
	"Serbia",
	"Seychelles",
	"Sierra Leone",
	"Singapore",
	"Sint Maarten (Dutch part)",
	"Slovakia",
	"Slovenia",
	"Solomon Islands",
	"Somalia",
	"South Africa",
	"South Georgia and the South Sandwich Islands",
	"South Sudan",
	"Spain",
	"Sri Lanka",
	"Sudan (the)",
	"Suriname",
	"Svalbard and Jan Mayen",
	"Sweden",
	"Switzerland",
	"Syrian Arab Republic",
	"Taiwan",
	"Tajikistan",
	"Tanzania, United Republic of",
	"Thailand",
	"Timor-Leste",
	"Togo",
	"Tokelau",
	"Tonga",
	"Trinidad and Tobago",
	"Tunisia",
	"Turkey",
	"Turkmenistan",
	"Turks and Caicos Islands (the)",
	"Tuvalu",
	"Uganda",
	"Ukraine",
	"United Arab Emirates (the)",
	"United Kingdom of Great Britain and Northern Ireland (the)",
	"United States Minor Outlying Islands (the)",
	"United States of America (the)",
	"Uruguay",
	"Uzbekistan",
	"Vanuatu",
	"Venezuela (Bolivarian Republic of)",
	"Viet Nam",
	"Virgin Islands (British)",
	"Virgin Islands (U.S.)",
	"Wallis and Futuna",
	"Western Sahara",
	"Yemen",
	"Zambia",
	"Zimbabwe",
	"Åland Islands"
];


  

const nationalities =  [
        "Afghan",
        "Albanian",
        "Algerian",
        "American",
        "Andorran",
        "Angolan",
        "Antiguans",
        "Argentinean",
        "Armenian",
        "Australian",
        "Austrian",
        "Azerbaijani",
        "Bahamian",
        "Bahraini",
        "Bangladeshi",
        "Barbadian",
        "Barbudans",
        "Batswana",
        "Belarusian",
        "Belgian",
        "Belizean",
        "Beninese",
        "Bhutanese",
        "Bolivian",
        "Bosnian",
        "Brazilian",
        "British",
        "Bruneian",
        "Bulgarian",
        "Burkinabe",
        "Burmese",
        "Burundian",
        "Cambodian",
        "Cameroonian",
        "Canadian",
        "Cape Verdean",
        "Central African",
        "Chadian",
        "Chilean",
        "Chinese",
        "Colombian",
        "Comoran",
        "Congolese",
        "Costa Rican",
        "Croatian",
        "Cuban",
        "Cypriot",
        "Czech",
        "Danish",
        "Djibouti",
        "Dominican",
        "Dutch",
        "East Timorese",
        "Ecuadorean",
        "Egyptian",
        "Emirian",
        "Equatorial Guinean",
        "Eritrean",
        "Estonian",
        "Ethiopian",
        "Fijian",
        "Filipino",
        "Finnish",
        "French",
        "Gabonese",
        "Gambian",
        "Georgian",
        "German",
        "Ghanaian",
        "Greek",
        "Grenadian",
        "Guatemalan",
        "Guinea-Bissauan",
        "Guinean",
        "Guyanese",
        "Haitian",
        "Herzegovinian",
        "Honduran",
        "Hungarian",
        "I-Kiribati",
        "Icelander",
        "Indian",
        "Indonesian",
        "Iranian",
        "Iraqi",
        "Irish",
        "Israeli",
        "Italian",
        "Ivorian",
        "Jamaican",
        "Japanese",
        "Jordanian",
        "Kazakhstani",
        "Kenyan",
        "Kittian and Nevisian",
        "Kuwaiti",
        "Kyrgyz",
        "Laotian",
        "Latvian",
        "Lebanese",
        "Liberian",
        "Libyan",
        "Liechtensteiner",
        "Lithuanian",
        "Luxembourger",
        "Macedonian",
        "Malagasy",
        "Malawian",
        "Malaysian",
        "Maldivian",
        "Malian",
        "Maltese",
        "Marshallese",
        "Mauritanian",
        "Mauritian",
        "Mexican",
        "Micronesian",
        "Moldovan",
        "Monacan",
        "Mongolian",
        "Moroccan",
        "Mosotho",
        "Motswana",
        "Mozambican",
        "Namibian",
        "Nauruan",
        "Nepalese",
        "New Zealander",
        "Ni-Vanuatu",
        "Nicaraguan",
        "Nigerian",
        "Nigerien",
        "North Korean",
        "Northern Irish",
        "Norwegian",
        "Omani",
        "Pakistani",
        "Palauan",
        "Panamanian",
        "Papua New Guinean",
        "Paraguayan",
        "Peruvian",
        "Polish",
        "Portuguese",
        "Qatari",
        "Romanian",
        "Russian",
        "Rwandan",
        "Saint Lucian",
        "Salvadoran",
        "Samoan",
        "San Marinese",
        "Sao Tomean",
        "Saudi",
        "Scottish",
        "Senegalese",
        "Serbian",
        "Seychellois",
        "Sierra Leonean",
        "Singaporean",
        "Slovakian",
        "Slovenian",
        "Solomon Islander",
        "Somali",
        "South African",
        "South Korean",
        "Spanish",
        "Sri Lankan",
        "Sudanese",
        "Surinamer",
        "Swazi",
        "Swedish",
        "Swiss",
        "Syrian",
        "Taiwanese",
        "Tajik",
        "Tanzanian",
        "Thai",
        "Togolese",
        "Tongan",
        "Trinidadian or Tobagonian",
        "Tunisian",
        "Turkish",
        "Tuvaluan",
        "Ugandan",
        "Ukrainian",
        "Uruguayan",
        "Uzbekistani",
        "Venezuelan",
        "Vietnamese",
        "Welsh",
        "Yemenite",
        "Zambian",
        "Zimbabwean"
]


