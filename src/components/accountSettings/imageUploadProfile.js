import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import { Icon,  TextInput, Button } from 'react-materialize';
// nao precisa mais : import avatar from '../../images/avatar.jpeg';
import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'

// componente para atualizar imagem do candidato
const API = url_internal_api.avatar_img;

class ImageUploadProfile extends Component {

  state ={
    selectedFile: null

  }

  fileSelectedHandler = event =>  {
    console.log(event.target.files[0])
    this.setState ({ selectedFile:  event.target.files[0]})
  }

  fileUploadHandler = () => {
    console.log("enviando para connexsxme")
    const fd = new FormData();
    fd.append('image', this.state.selectedFile, this.state.selectedFile.name);
    axios.post(API,fd)
    .then (res => {
      console.log(res);
    
    });

  }

  render(){
    return (
      <>
       <TextInput
    id="image_file"
    label="File"
    type="file"
    
    onChange ={this.fileSelectedHandler}
    />
    <Button onClick={this.fileUploadHandler}
     floating
     icon={<Icon>add</Icon>}
     small
     node="button" />
     
    </>
    )


  }
 

} 
export default ImageUploadProfile;