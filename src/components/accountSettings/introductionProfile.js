/**
 * Componente e manipulacao dos eventos para 
 * Apresentacao do usuario 
 * nome , email e foto
 * 
 */

import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import { TextInput,   Col,   Button } from 'react-materialize';
import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'




const API = url_internal_api.introduction_values;
// TODO enviar o token para o servidor e entao extrair o id_usuario / email

export default class IntroductionProfile extends Component {
  

  constructor(){ 
      super();
        this.state = {   // https://blog.stvmlbrn.com/2017/04/07/submitting-form-data-with-react.html
          id_usuario: '',
          name_first: '',
          name_last: '',
          email: '',
          hits: [],
          loading:true
           }
    }

  /**
   * este metodo armazena o estado do que for preenchido no formulario
   */
  handleChange = event => {   //https://alligator.io/react/axios-react/
     //console.log("handle change  value:")
     console.log(event.target.name + " " + event.target.value);
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = event => {
        event.preventDefault();
        const { name_first, name_last, email, hits } = this.state;
        console.log("conteudo do formulario:");
        console.log(event.target.elements); // coleta somente elementos do formulario
        console.log(event.target.name_first.value); // coleta somente elementos do formulario
        console.log(event.target.name_last.value); // coleta somente elementos do formulario

        console.log("conteudo do formulario. itens checkbox:");
        var todosElementosDoForm =  this.state;
        
        
        
        console.log("todosElementosDoForm");
        console.log(todosElementosDoForm);

        const newIntroduction = todosElementosDoForm

        axios.post(API, { newIntroduction })
          .then(res => {
            console.log("enviando POST introduction profile values resposta: ");
            console.log(res);
            console.log(res.data);
          })
  }


    /**
     * apos renderizar então busca dados via API para atualizar o formulario
     */
   async componentDidMount() {
    const response = await  fetch(API+'?email=tamer@tamerico.com');
    const data =  await response.json();   // https://javascript.info/fetch
    console.log("response from "+API + " data  values: ");
    console.log(data);  // conteudo entre []
    console.log(data[0]); // extracao do conteudo para json
    this.setState(   
      {
        id_usuario: data[0]['id_usuario'],
        name_first: data[0]['name_first'],
        name_last: data[0]['name_last'],
        email: data[0]['email'],
        hits : data[0] ,
         loading:false}
      //{avatar: data , loading:false}
    );
    
  }

  

  render(){
    const { name_first, name_last, email, hits } = this.state;
    console.log("hits original tamanho: "+ hits.length) 
     console.log("email ? : "+ name_first ) 
     console.log("email ? : "+ name_last ) 
        
    
    return (
      <>
      
        {this.state.loading || !this.state.hits ?(
            <div>loading...</div>
          ):(
          
              <div>
                <form id="valuesformIntroduction" onSubmit={this.handleSubmit}>
                {/*  solucao para input com value = null : https://github.com/mui-org/material-ui/issues/4904 */}
                  <TextInput id="firstName" placeholder="First Name" type="text" label="FirstName" name="name_first"  s={12} value={name_first} onChange={this.handleChange} />
                  <TextInput id="lastName"  placeholder="Last Name"  type="text" label="LastName"  name="name_last"   s={12} value={name_last}  onChange={this.handleChange} />
                  
                  <TextInput disabled id="email"  label="e-mail" s={12} value={hits.email} />

                <Col s={12} m={12}>
                  <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
                  </Col>
              </form>
                </div>

          

          )
        }
      </>
    )


  }
 

} 
