/**
 * Componente e manipulacao dos eventos para 
 * Relocation
 * Sao 2 campos:
 *  um de select que responde uma pergunta e 3 opcoes disponiveis
 *  multiplos checkcox para selecionar paises na europa, agurpados por regiao
 * Serao utilizados para pesquisar vagas e empresas que combinem com
 * os paises  selecionados e tipo de trabalho: remoto ou presencial ou misto
 */

import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import {Checkbox, Col, Button, Select } from 'react-materialize';

import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'
const API = url_internal_api.relocation;

// TODO enviar o token para o servidor e entao extrair o id_usuario / email

export default class RelocationProfile extends Component {
  
  state = {
    hits: [],// data from relocation table
    remote: '',
    remote_text: 'Choose your option',
    loading:true,
  }



  /**
   * este metodo armazena o estado do que for preenchido no formulario
   */
  handleChangeRemote = event => {   //https://alligator.io/react/axios-react/
    console.log("remote y/n/both handle change  value:")
    console.log(">>" + event.target.name + " <<" + event.target.value + "--");
    
  this.setState({  "remote": event.target.value });
  
  }


  
  /**
   * 
   * manipula dados do form para enviar via metodo post
   */
  handleSubmit = event => {
        event.preventDefault();
        console.log("conteudo do formulario:");
        console.log(event.target.elements); // coleta somente elementos do formulario

        console.log("conteudo do formulario. itens checkbox:");
        var todosElementosDoForm = event.target.elements;

        
        var tipoRemoto = this.state.remote;
        var elementoPraAvaliar;
        var checkboxForPOST = {};
        for (elementoPraAvaliar in todosElementosDoForm){
          
          var elementoAvaliar = todosElementosDoForm[elementoPraAvaliar].type;
          
          // coletar somente os campos checkbox
          if (elementoAvaliar == 'checkbox')
          {
            var idAtual = todosElementosDoForm[elementoPraAvaliar].id
            var checkedStatusAtual = todosElementosDoForm[elementoPraAvaliar].checked;
            console.log (elementoAvaliar + " id: " + idAtual  + " -- checked: " + checkedStatusAtual);
            

             // cada checkbox vira um par key=value
             checkboxForPOST[idAtual]=checkedStatusAtual
          }
          
        }


        
        console.log("checkboxForPOST");
        console.log(checkboxForPOST);

        const newRelocationValues = checkboxForPOST

        axios.post(API, { newRelocationValues, tipoRemoto })
          .then(res => {
            console.log("enviando POST personal _ values resposta: ");
            console.log(res);
            console.log(res.data);
          })
  }




  async componentDidMount() {
    const response = await  fetch(API+'?email=tamer@tamerico.com');
    const data =  await response.json();   // https://javascript.info/fetch
    console.log("response from "+API + " data  values: ");
    console.log(data);
    console.log("relocation values: ");
    console.log(data.relocation);

    let data_relocation_remote = data.relocation[0].remote;
    console.log("data_relocation_remote: "+ data_relocation_remote);

    let data_relocation_remote_default_text = "Choose your option";

    if ((data_relocation_remote == null) || (data_relocation_remote.length==0))
      data_relocation_remote = "Choose your option";
    else{
      data_relocation_remote_default_text = data_relocation_remote;

    } 



    console.log("data_relocation_remote after: "+ data_relocation_remote);

    

    let data_relocation_countries_relocation = data.relocation[0].countries.relocation;
    console.log("conteudo para state");
    console.log(data.relocation[0].countries.relocation);


    

    this.setState(   
      {
        hits : data_relocation_countries_relocation,
        remote: data_relocation_remote,
        remote_text: data_relocation_remote_default_text,
        loading:false}
      //{avatar: data , loading:false}
    );
    // console.log(data.results[0]);
  }

  
  


  render(){
    const { hits } = this.state;
    console.log("hits original tamanho: "+(hits).length)
    
    

    const regiaoFiltrada = hits;
    const regiaoEastern = [];
    const regiaoNorthen = [];
    const regiaoSouthern = [];
    const regiaoWestern = [];

    for (var re in regiaoFiltrada)
    {
      var pais = JSON.stringify (regiaoFiltrada[re]);
      pais = JSON.parse (pais);
      var region = pais.region;
      

      if (region == ("Eastern Europe"))
      {  //console.log (pais);
        regiaoEastern[re] = regiaoFiltrada[re];
      }

      if (region == ("Northern Europe"))
      { // console.log (pais);
        regiaoNorthen[re] = regiaoFiltrada[re];
      }

      if (region == ("Southern Europe"))
      { // console.log (pais);
        regiaoSouthern[re] = regiaoFiltrada[re];
      }

      if (region == ("Western Europe"))
      { // console.log (pais);
        regiaoWestern[re] = regiaoFiltrada[re];
      }


    }
    console.log (regiaoEastern);
    console.log (regiaoNorthen);
    console.log (regiaoSouthern);
    console.log (regiaoWestern);


    return (
      <>
      {this.state.loading || !this.state.hits ?(
          <div>loading...</div>
        ):(
        //   <ul>
            
        //   {hits.map(hit =>
        //     <li key={hit.objectId}>
        //       <a href={hit.description}>xx__din_{hit.checked}aaaaaaa</a>
        //     </li>
        //   )}
        //  </ul>
        <div>
          <form id="valuesformRelocation" onSubmit={this.handleSubmit}>

          <div className="row">
            <div className="col">
            <Select
                    label="Area you open to remote jobs?"
                    onChange = {this.handleChangeRemote}
                    id="remote"
                    multiple={false}
                    options={{
                      classes: '',
                      dropdownOptions: {
                        alignment: 'left',
                        autoTrigger: true,
                        closeOnClick: true,
                        constrainWidth: true,
                        coverTrigger: true,
                        hover: false,
                        inDuration: 150,
                        onCloseEnd: null,
                        onCloseStart: null,
                        onOpenEnd: null,
                        onOpenStart: null,
                        outDuration: 250
                      }
                    }}
                    value={this.state.remote}
                  >
                    <option
                      disabled
                      value=""
                    >
                      {this.state.remote_text} 
                    </option>

                    <option value="Yes">
                     Yes
                    </option>

                    <option value="No">
                     No
                    </option>

                    <option value="Remote only">
                     Remote only
                    </option>
                 
                </Select>
            </div>
          </div>

          <div className="row">
          <h6>Select the desired country(ies) to relocation or work remote for</h6>
          </div>

          <div className="row">

            <div className="col">
                <p key="Eastern Europe">Eastern Europe</p>
              {regiaoEastern.map
                
                (hit =>
                  <p key={hit.objectId} >
                  <Checkbox
                  
                  onChange={this.handleChange}
                  checked={hit.checked}
                  key = {hit.objectId}
                  id={hit.objectId}
                  label={hit.description}
                  value={hit.description}
                  />
                  </p>

                )}

            </div>

            <div className="col">

                <p key="Northern Europe">Northern Europe</p>
              {regiaoNorthen.map
                
                (hit =>
                  <p key={hit.objectId} >
                  <Checkbox
                  
                  onChange={this.handleChange}
                  checked={hit.checked}
                  key = {hit.objectId}
                  id={hit.objectId}
                  label={hit.description}
                  value={hit.description}
                  />
                  </p>

                )}
            </div>

            <div className="col">
              <p key="Southern Europe">Southern Europe</p>
              {regiaoSouthern.map
                
                (hit =>
                  <p key={hit.objectId} >
                  <Checkbox
                  
                  onChange={this.handleChange}
                  checked={hit.checked}
                  key = {hit.objectId}
                  id={hit.objectId}
                  label={hit.description}
                  value={hit.description}
                  />
                  </p>

                )}

            </div>

            <div className="col">
              <p key="Western Europe">Western Europe</p>
              {regiaoWestern.map
                  
                  (hit =>
                    <p key={hit.objectId} >
                    <Checkbox
                    
                    onChange={this.handleChange}
                    checked={hit.checked}
                    key = {hit.objectId}
                    id={hit.objectId}
                    label={hit.description}
                    value={hit.description}
                    />
                    </p>

                  )}
            </div>
          </div>


          
           
         
          <Col s={12} m={12}>
             <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
             </Col>
        </form>
          </div>

         

        )}
      </>
    )


  }
 

} 
