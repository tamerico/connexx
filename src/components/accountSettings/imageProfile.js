import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import url_internal_api from '../../configs/urlAPI'


// componente para atualizar imagem do candidato
//const API = 'http://developer:9001/avatar?email=tamer@tamerico.com';
const API = url_internal_api.avatar_img + '?email=tamer@tamerico.com';

class ImageProfile extends Component {

  

  state = {
    loading:true,
    avatar: null


  }

   async componentDidMount() {
     const response = await  fetch(API);
     const data =  await response.text();   // https://javascript.info/fetch
     console.log("response from: "+ API + ":");
     if (data.length>50)
      console.log(data.slice(0,50)+ "...  ");
      else
       console.log(data);
     this.setState({avatar: data , loading:false});
    // console.log(data.results[0]);
  }

  

  render(){
    return (
      <>
      {this.state.loading || !this.state.avatar ?(
          <div>loading...</div>
        ):(
          <img
              alt=""
              className="circle right"
              style={{height: 120}}
              src={`data:image;base64,${this.state.avatar}`}  // https://stackoverflow.com/questions/42395034/how-to-display-binary-data-as-image-in-react
              />
        )}
      </>
    )


  }
 

} 
export default ImageProfile;