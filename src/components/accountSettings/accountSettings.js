/*
 * Este componente define o layout de sub componentes referentes a 
 * personalização/configuração dos dados do candidato

*/


// Importando o React
// http://react-materialize.github.io/react-materialize/?path=/story/components-footer--default
// https://react-icons.github.io/

// questionario Sandra https://docs.google.com/forms/d/15ia8ajTJq0kiGJ6FFbRi0PI-Qe5L15xBZ3gpaomlhGc/edit?ts=5ea92d75#responses
//Apart from salary, which benefits are most attractive to you, with regards to a job in a foreign country? Please choose the 3 most important for you





import React from "react";
// Importando os components necessários da lib react-materialize
import { Select, Icon,  Row, Col, Card, TextInput, Button, Modal } from 'react-materialize';


import cefrBeg  from '../../images/cefr_beg.png';
import cefrInt  from '../../images/cefr_int.png';
import cefrAdv  from '../../images/cefr_adv.png';


// import Favorite from '@material-ui/icons/Favorite';
// import FavoriteBorder from '@material-ui/icons/FavoriteBorder';

// Importando o componenet UserProfile
import ImageUploadProfile from './imageUploadProfile'
import ImageProfile from "./imageProfile";
import IntroductionProfile from "./introductionProfile"
import PersonalValuesProfile from "./personalValuesProfile"
import BenefitsProfile from "./benefitsProfile"
import Citizenship  from "./citizenshipProfile";
import Languages  from "./languagesProfile";
import RelocationProfile from "./relocationProfile";
import ResumeLinkProfile from "./resumeProfile";

  



const AccountSettings = () => (
    
  <Row>
   
    <Col m={12} s={12}>
        <h4>Your settings</h4>
    </Col>
    <Col m={8} s={8} className="right">
        <Card >
                   
          <Row>
            <ImageProfile/>
            <ImageUploadProfile/>
          
            <IntroductionProfile/>
            
            

          </Row>
        </Card>
    </Col>
  

  




   <Col m={12} s={12}>
       <h4>Citizenship</h4>
   </Col>
   <Col m={8} s={8} className="right">
       <Card >
                  
         <Row  >
        
              <Citizenship/>
         
         </Row>
       </Card>
   </Col>


   
   <Col m={12} s={12}>
       <h4>Languages (CEFR based)</h4>
              <Modal
          actions={[
            <Button flat modal="close" node="button" waves="green">Close</Button>
          ]}
          bottomSheet={false}
          fixedFooter={false}
          header="The CEFR Standard"
          id="Modal-0"
          open={false}
          options={{
            dismissible: true,
            endingTop: '10%',
            inDuration: 250,
            onCloseEnd: null,
            onCloseStart: null,
            onOpenEnd: null,
            onOpenStart: null,
            opacity: 0.5,
            outDuration: 250,
            preventScrolling: true,
            startingTop: '4%'
          }}
          
          trigger={<Button node="button" className="btn-flat" >about CEFR</Button>}
        >
          <div className="container container-fluid">
            <div className="row">
            <div className="col"><img src={cefrBeg} alt="Cef Beg" style={{height: 120}}/></div>
            <div className="col"><img src={cefrInt} alt="Cef Int" style={{height: 120}}/></div>
            <div className="col"><img src={cefrAdv} alt="Cef Adv" style={{height: 120}}/></div>
            </div> 
            <div className="row">
              <p>
              The CEFR (Common European Framework of Reference) is a language proficiency standard that classifies learners into beginner, intermediate, and advanced levels of competence with respect to the language that they’re learning. These levels are conventionally labeled A1, A2, B1, B2, C1, and C2—corresponding to the various levels of proficiency as pictured above. The level of a learner is assessed with respect to communicative competences in listening, reading, writing, and speaking by evaluation learners according to “Can do…” statements. These statements range from “Can understand and produce basic greetings” (at a beginner level) all the way up through “Can produce well-structured detailed text on complex subjects” (at a more advanced level).
              </p>
              </div>
          </div>

          
          
        </Modal>
   </Col>
   <Col m={8} s={8} className="right">

       <Card >
          <Row  >
              <Languages/>
            </Row>
       </Card>
    </Col>




   <Col m={12} s={12}>
       <h4>Relocation</h4>
   </Col>
   <Col m={8} s={8} className="right">
       <Card >
                  
           {/* <Row  style={{margin: -10 + 'px'}} >
            Area you open to remote jobs?
            </Row> */}
            <Row>
                    <RelocationProfile/>
            </Row>
                       
        
       </Card>
   </Col>
 

   
  


   
   <Col m={12} s={12}>
       <h4>Your Resumé</h4>
   </Col>
   <Col m={8} s={8} className="right">
       <Card >
                  
         <Row  >
         {/* <ResumeLinkProfile/> */}
         {/* <ResumeFileUploadProfile/> */}
        
          </Row>
            <Row>
            <h6>We won't ask you to fill up an entire Resumé but... with some resumé details we can find more valueable offer jobs and matched profetionals</h6>
           
            <ResumeLinkProfile/>

           
         </Row>
       </Card>
   </Col>



   <Col m={12} s={12}>
       <h4>Values</h4>
   </Col>
   <Col m={8} s={8} className="right">
       <Card >
                  
         <Row  >
         <PersonalValuesProfile/>
           
         </Row>
       </Card>
   </Col>
 



   <Col m={12} s={12}>
       <h4>Benefits</h4>
   </Col>
   <Col m={8} s={8} className="right">
       <Card >
                  
         <Row  >
         <BenefitsProfile/>
         
         </Row>
       </Card>
   </Col>
 

   </Row>


);

export default AccountSettings;