/**
 * Componente e manipulacao para
 * Beneficios do candidato
 * pode selecionar 3 beneficios 
 */

import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import { Icon, Checkbox ,  Col,   Button } from 'react-materialize';
import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'



const API = url_internal_api.benefits;
// TODO enviar o token para o servidor e entao extrair o id_usuario / email

export default class BenefitsProfile extends Component {
  
  state = {
    hits: [],
    loading:true,
  }


  handleChange = event => {   //https://alligator.io/react/axios-react/
    // console.log("handle change checkbox personal value:")
    // console.log(event);
    // console.log(event.target.checked);
    // console.log(event.target.value);
  }

  handleSubmit = event => {
        event.preventDefault();
        console.log("conteudo do formulario:");
        console.log(event.target.elements); // coleta somente elementos do formulario

        console.log("conteudo do formulario. itens checkbox:");
        var todosElementosDoForm = event.target.elements;
        var elementoPraAvaliar;
        var checkboxForPOST = {};
        for (elementoPraAvaliar in todosElementosDoForm){
          
          var elementoAvaliar = todosElementosDoForm[elementoPraAvaliar].type;
          
          // coletar somente os checkbox
          if (elementoAvaliar == 'checkbox')
          {
            var idAtual = todosElementosDoForm[elementoPraAvaliar].id
            var checkedStatusAtual = todosElementosDoForm[elementoPraAvaliar].checked;
            console.log (elementoAvaliar + " id: " + idAtual  + " -- checked: " + checkedStatusAtual);
            

             // cada checkbox vira um par key=value
             checkboxForPOST[idAtual]=checkedStatusAtual
          }
          
        }
        console.log("checkboxForPOST");
        console.log(checkboxForPOST);

        const newBenefits = checkboxForPOST

        axios.post(API, { newBenefits })
          .then(res => {
            console.log("enviando POST desired benefits resposta: ");
            console.log(res);
            console.log(res.data);
          })
  }

   async componentDidMount() {
    const response = await  fetch(API+'?email=tamer@tamerico.com');
    const data =  await response.json();   // https://javascript.info/fetch
    console.log("response from "+API + " data  values: ");
    console.log(data);
    console.log("data desired benefits: ");
    console.log(data.benefits);
    this.setState(   
      {hits : data.benefits , loading:false}
      //{avatar: data , loading:false}
    );
    // console.log(data.results[0]);
  }

  

  render(){
    const { hits } = this.state;
    console.log("hits original tamanho: "+(hits).length)
    
    
    return (
      <>
      <h6>Desired benefits (select the 3 most important)</h6>
      {this.state.loading || !this.state.hits ?(
          <div>loading...</div>
        ):(
        //   <ul>
            
        //   {hits.map(hit =>
        //     <li key={hit.objectId}>
        //       <a href={hit.description}>xx__din_{hit.checked}aaaaaaa</a>
        //     </li>
        //   )}
        //  </ul>
        <div>
          <form id="valuesformid" onSubmit={this.handleSubmit}>
          {hits.map
            (hit =>
              <p key={hit.objectId} >
              <Checkbox
              
              onChange={this.handleChange}
              checked={hit.checked}
              key = {hit.objectId}
              id={hit.objectId}
              label={hit.description}
              value={hit.description}
              />
              </p>
            )}
          <Col s={12} m={12}>
             <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
             </Col>
        </form>
          </div>

         

        )}
      </>
    )


  }
 

} 
