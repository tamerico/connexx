/**
 * Componente e manipulacao dos eventos para 
 * dados curriculum vitae  e complementares
 * url do curriculum (pdf ou json resume)
 * linkedin url  + foto + response 200
 * desired position 
 * seniority level
 * yeras of experience
 * desired sector
 */

import React, { Component } from "react";
// Importando os components necessários da lib react-materialize
import { TextInput, Col, Button, Icon } from 'react-materialize';
import Autocomplete from '../autocompleteComponent/autocompleteComponent'  // personalizacacao do componente para poder se comunicar com parent<>child.
import axios from 'axios';
import url_internal_api from '../../configs/urlAPI'




const API = url_internal_api.resume;
// TODO enviar o token para o servidor e entao extrair o id_usuario / email

export default class ResumeProfile extends Component {
  

  constructor(){ 

    super();

    this.industryElement = React.createRef()// para enviar dados para o child https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    this.skills_keywordsElement = React.createRef()// para enviar dados para o child https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/

    var skills =  [ {language_1: '',
    language_2: '',
    language_3: '',
    language_4: '',
    language_5: '',
    language_6: '',
    language_7: '',
    language_8: '',
    language_9: '',
    language_10: ''}];

    this.state = {   // https://blog.stvmlbrn.com/2017/04/07/submitting-form-data-with-react.html
      url_linkedin: '',
      url_resume: '',
      job_position: '',
      industry:'',
      seniority_level:'',
      years_experience: '',
      skills_keywords: skills,
      loading:true
        }

      console.log(this.state);
  }

  /**
   * este metodo armazena o estado do que for preenchido no formulario
   */
  handleChange = event => {   //https://alligator.io/react/axios-react/
     //console.log("handle change  value:")
    console.log("entrada: " + event.target.name + " " + event.target.value);
    this.setState({ [event.target.name]: event.target.value });
    

  }

  /**
   * limita para 2 o numero de caracteres
   */
  handleChangeNumber = event => {   //https://alligator.io/react/axios-react/
    //console.log("handle change  value:")
   console.log("entrada: " + event.target.name + " " + event.target.value);
   
   /** O tipo número para o campo years_experience nao tem suporte ao parametro maxLength. Desta forma fiz esse limitador: */
   if (event.target.name == "years_experience")  
     if ((event.target.value).length >2 )  
       console.log("max >2");
     else
     this.setState({ [event.target.name]: event.target.value });
   
   console.log("limit 2: " + event.target.name + " " + this.state.years_experience);


 }



    // metodo especifico para o compoente Autocomplete 
  handleChangeIndustry = (val) => {   //https://alligator.io/react/axios-react/
    console.log("handle change  value:")
    console.log(val );
  
    // define estado que é resgatado do componente filho
    this.setState({  "industry": val}); // update FROM child.  gracias  https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
  }


  handleSubmit = event => {
        event.preventDefault();
        const { name_first, name_last, email, hits } = this.state;
        console.log("conteudo do formulario:");
        console.log(event.target.elements); // coleta somente elementos do formulario
        // console.log(event.target.name_first.value); // coleta somente elementos do formulario
        // console.log(event.target.name_last.value); // coleta somente elementos do formulario

        
        var todosElementosDoForm =  this.state;
        
        
        
        console.log("todosElementosDoForm");
        console.log(todosElementosDoForm);

        const newResume = todosElementosDoForm

        axios.post(API, { newResume }) // Desse modo cria-se uma estrutura json com o parametro pai com o mesmo nome da variavel: newResume e em seguida todos os elementos do form
          .then(res => {
            console.log("enviando POST introduction profile values resposta: ");
            console.log(res);
            console.log(res.data);
          })
  }


  // metodo especifico para o componente Autocomplete 
  prepareNewLanguageButton = (val) => {   //https://alligator.io/react/axios-react/
      console.log("add button language...:");
      console.log(val );
    
      // TODO  verifica o pool de botoes qual é o tamanho.
      // verifica o primeiro key/value vazio e adicionar o valor para ele
      const poolLanguages = this.state.skills_keywords;
      console.log(poolLanguages);
      // if (typeof((poolLanguages['loading']) == 'undefined') )
      //   console.log("ja apaguei 1 vez. ");
      // else
      //   delete poolLanguages['loading'];

      for (var poolX in poolLanguages[0]) 
      {
        console.log("pool x= "+ poolX) 
        if ( poolX.startsWith("language_"))
          if  ( (poolLanguages[0][poolX]==null ) ||  (poolLanguages[0][poolX]=="" )  )
          {
            console.log(poolLanguages);
            console.log (poolX + " esta livre");
            console.log (poolX + " atribuir valor :" + val);
            const novoSkill_ADD= { skills_keywords:  poolLanguages}; //precisa ser um array com um elemento
            console.log(novoSkill_ADD);
            // erro novoSkill['skills_keywords'][poolX]=val;
            novoSkill_ADD['skills_keywords'][0][poolX]=val;
             console.log(novoSkill_ADD);
             
            this.setState(
                               [ { skills_keywords : novoSkill_ADD} ]
                        );
              console.log(this.state);
            //this.setState( { poolLanguages :{ [poolX]: val}});
            break;
          }
          else console.log (poolX + " esta ocupado :" + poolLanguages[poolX]);
      }
      console.log("fim do add buttons");
      
  }




  removeButtonLanguage = (e) => {   //https://alligator.io/react/axios-react/
      // https://reactjs.org/docs/handling-events.html   sobre synthetic events = e
    
    var poolLanguages_Remove = this.state.skills_keywords;
    
    var novoSkill_Remove= { skills_keywords:  poolLanguages_Remove};
    console.log(novoSkill_Remove);
    
    novoSkill_Remove['skills_keywords'][0][e.target.id]="";
    
    this.setState( 
                       [{  skills_keywords: novoSkill_Remove } ]
            );

    console.log(this.state);

  }


  // Gera o source para os botoes a partir do retorno dos dados do banco de dados
  // gera lista somente com elementos que possuem conteudo
  generateButtons (){
      console.log("dentro da funcao generateButtons");
      var LanguageButtonElements = {};
      console.log(this.state.skills_keywords);
      const hits = this.state.skills_keywords[0];

      console.log( (hits));

      console.log( typeof (hits));
      for (const propriedade in hits){
          console.log(propriedade);
          if (propriedade.startsWith("language"))
          {
              console.log( '>>> ' + propriedade + ": " + hits[propriedade] );
              
              if ( hits[propriedade] != null)
                {
                  if ( hits[propriedade].length > 2)  // só pegar os que possuem conteudo
                {
                    // LanguageButtonElements.push(hits[propriedade]);
                  console.log( '### ' + propriedade + "= " + hits[propriedade] );
                  LanguageButtonElements[propriedade] = (hits[propriedade]);
                }
                else
                  delete LanguageButtonElements[propriedade];
                }
                else console.log("fucking null: "+propriedade );
          }


      }
      console.log( (LanguageButtonElements));
      

      return LanguageButtonElements;     
      

  }




    /**
     * apos renderizar então busca dados via API para atualizar o formulario
     */
   async componentDidMount() {
    const response = await  fetch(API+'?email=tamer@tamerico.com');
    const data =  await response.json();   // https://javascript.info/fetch
    console.log("response from "+API + " data  values: ");
    console.log(data);  // conteudo entre []
    console.log(data[0]); // extracao do conteudo para json



    // var mock_skills =  [{language_1: '',
    // language_2: 'tamer',
    // language_3: 'jornda',
    // language_4: 'aely',
    // language_5: 'maya',
    // language_6: 'rosura',
    // language_7: 'claudia',
    // language_8: 'sarah',
    // language_9: 'coelho',
    // language_10: 'galinhas'}];

    this.setState(   
      {
        url_linkedin: data[0]['url_linkedin'],
        url_resume: data[0]['url_resume'],
        job_position: data[0]['job_position'],   
        industry: data[0]['industry'],
        seniority_level: data[0]['seniority_level'],
        years_experience: data[0]['years_experience'],
        skills_keywords: data[0]['skills_keywords'],
        loading:false}
      //{avatar: data , loading:false}
    );
    console.log(this.state);
    this.industryElement.current.updateInputValue(data[0]['industry'], "Industry"); // update from Parent TO child. gracias https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    //this.skills_keywords.current.updateInputValue(data[0]['skills_keywords'], "preenche um de cada vez"); // update from Parent TO child. gracias https://www.freecodecamp.org/news/react-changing-state-of-child-component-from-parent-8ab547436271/
    console.log(this.state.industry);
    
  }

  

  render(){
    //const { url_linkedin, url_resume, job_position, industry, seniority_level } = this.state;
    
    const  eleToRender = this.generateButtons();
    console.log(" tamanho do ele to render "+ Object.keys(eleToRender).length);
    console.log(typeof(eleToRender));
    console.log((eleToRender));
    var tagButtonElements = [];

    for (var botaoX in eleToRender)
    //for (var i=0; i<eleToRender.length; i++)
    { 
      
       
        console.log("" + botaoX);
        
        tagButtonElements.push(
          // interessante observar que a definicao de uma funcao no onClick nao pode ter parenteses()
          // https://stackoverflow.com/questions/38401902/onclick-not-working-react-js
          <p key={botaoX}><Button  key={botaoX} id={botaoX} type="button" waves='light' 
                  onClick={(e) => this.removeButtonLanguage(e)} >{eleToRender[botaoX]}
            <Icon key={botaoX} right >
            {/* TODO adicionar evento ou stop propagagion ou similar para que o icone, ao ser clicado, execute a funcao do parent 
              avaliar https://stackoverflow.com/questions/38861601/how-to-only-trigger-parent-click-event-when-a-child-is-clicked */}
              close
            </Icon>
          </Button>
          </p>
          );
      }



    
    return (
      <>
      
        {this.state.loading  ?(
            <div>loading...</div>
          ):(
          
              <div>
                <form id="valuesformIntroduction" onSubmit={this.handleSubmit}>
                {/*  solucao para input com value = null : https://github.com/mui-org/material-ui/issues/4904 */}
                  <TextInput id="url_linkedin" placeholder="Linkedin URL" type="text" label="Linkedin url" 
                        name="url_linkedin"  s={12} value={this.state.url_linkedin} onChange={this.handleChange} />

                  {/* aqui compara resume com cv : https://www.thebalancecareers.com/cv-vs-resume-2058495 */}
                  <TextInput id="url_resume" placeholder="Resume/Curriculum URL (google drive or another public url)" type="text"
                       label="Resume/Curriculum url" name="url_resume"  s={12} value={this.state.url_resume} onChange={this.handleChange} />

                  <TextInput id="job_position" placeholder="Job position" type="text"
                       label="Desired Job Position" name="job_position"  s={12} value={this.state.job_position} onChange={this.handleChange} />
                  
                  <Autocomplete
                      ref = {this.industryElement} // iso é para atualizar o child apos pegar dados do DB
                      tamer = {this.handleChangeIndustry}   //  update FROM child. isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
                      ownSuggestion = {true}  // informa o child para aceitar qualquer palavra
                      suggestions={industries}
                    />

                  <TextInput id="seniority_level" placeholder="seniority level type name" type="text"
                       label="Seniority Level" name="seniority_level"  s={12} value={this.state.seniority_level} onChange={this.handleChange} />

                  <TextInput id="years_experience" placeholder="Years of experience" type="number" 
                   label="Years of experience" name="years_experience"  s={12} value={this.state.years_experience} onChange={this.handleChangeNumber} />
                  
                  {/* site interessante com lista de varias positions em varios campos como exemplo bancos e hospitais:
                    https://www.thebalancecareers.com/healthcare-medical-job-titles-2061494 
                    https://www.thebalancecareers.com/top-job-titles-in-the-banking-industry-2061514
                    https://www.thebalancecareers.com/media-job-titles-2061502

                    https://zety.com/blog/job-titles

                   
                    */}
                  
                 

                  <Autocomplete
                tamer = {this.prepareNewLanguageButton}   //  update FROM child. isso é essencial para pegar o texto . gracias https://stackoverflow.com/questions/35537229/how-to-update-parents-state-in-react
                suggestions={skillsButNotLimited}
                />

                  {tagButtonElements}
                 

                <Col s={12} m={12}>
                  <Button type="submit" waves='light' className="right grey darken-2">Update</Button>
                  </Col>
              </form>
                </div>

          

          )
        }
      </>
    )


  }
 

} 


const industries = ['Accounting', 'Airlines/Aviation', 'Alternative Dispute Resolution', 'Alternative Medicine', 'Animation', 'Apparel & Fashion', 'Architecture & Planning', 'Arts and Crafts', 'Automotive', 'Aviation & Aerospace', 'Banking', 'Biotechnology', 'Broadcast Media', 'Building Materials', 'Business Supplies and Equipment', 'Capital Markets', 'Chemicals', 'Civic & Social Organization', 'Civil Engineering', 'Commercial Real Estate', 'Computer & Network Security', 'Computer Games', 'Computer Hardware', 'Computer Networking', 'Computer Software', 'Construction', 'Consumer Electronics', 'Consumer Goods', 'Consumer Services', 'Cosmetics', 'Dairy', 'Defense & Space', 'Design', 'Education Management', 'E-Learning', 'Electrical/Electronic Manufacturing', 'Entertainment', 'Environmental Services', 'Events Services', 'Executive Office', 'Facilities Services', 'Farming', 'Financial Services', 'Fine Art', 'Fishery', 'Food & Beverages', 'Food Production', 'Fund-Raising', 'Furniture', 'Gambling & Casinos', 'Glass, Ceramics & Concrete', 'Government Administration', 'Government Relations', 'Graphic Design', 'Health, Wellness and Fitness', 'Higher Education', 'Hospital & Health Care', 'Hospitality', 'Human Resources', 'Import and Export', 'Individual & Family Services', 'Industrial Automation', 'Information Services', 'Information Technology and Services', 'Insurance', 'International Affairs', 'International Trade and Development', 'Internet', 'Investment Banking', 'Investment Management', 'Judiciary', 'Law Enforcement', 'Law Practice', 'Legal Services', 'Legislative Office', 'Leisure, Travel & Tourism', 'Libraries', 'Logistics and Supply Chain', 'Luxury Goods & Jewelry', 'Machinery', 'Management Consulting', 'Maritime', 'Market Research', 'Marketing and Advertising', 'Mechanical or Industrial Engineering', 'Media Production', 'Medical Devices', 'Medical Practice', 'Mental Health Care', 'Military', 'Mining & Metals', 'Motion Pictures and Film', 'Museums and Institutions', 'Music', 'Nanotechnology', 'Newspapers', 'Non-Profit Organization Management', 'Oil & Energy', 'Online Media', 'Outsourcing/Offshoring', 'Package/Freight Delivery', 'Packaging and Containers', 'Paper & Forest Products', 'Performing Arts', 'Pharmaceuticals', 'Philanthropy', 'Photography', 'Plastics', 'Political Organization', 'Primary/Secondary Education', 'Printing', 'Professional Training & Coaching', 'Program Development', 'Public Policy', 'Public Relations and Communications', 'Public Safety', 'Publishing', 'Railroad Manufacture', 'Ranching', 'Real Estate', 'Recreational Facilities and Services', 'Religious Institutions', 'Renewables & Environment', 'Research', 'Restaurants', 'Retail', 'Security and Investigations', 'Semiconductors', 'Shipbuilding', 'Sporting Goods', 'Sports', 'Staffing and Recruiting', 'Supermarkets', 'Telecommunications', 'Textiles', 'Think Tanks', 'Tobacco', 'Translation and Localization', 'Transportation/Trucking/Railroad', 'Utilities', 'Venture Capital & Private Equity', 'Veterinary', 'Warehousing', 'Wholesale', 'Wine and Spirits', 'Wireless', 'Writing and Editing'];

const skillsButNotLimited =  ["Java", "Postgres", "Award", "Wood Crafter skills", "good father", "good man", "atractive", "react js",
 "ps4 gamer", "swimmer", "love mondays", "thansk god", "love my wife", "I will live in europe", "I love me", "Afrikaans A2", "Afrikaans B1", "Afrikaans B2", "Afrikaans C1", "Afrikaans C2"];