import React from 'react';
import { useAuth0 } from "@auth0/auth0-react";   // instalado com npm install @auth0/auth0-react

// import Loading from './components/any/loading';
import Header from './components/header/header'
import Main from './main'

function App() {   //  mesmo que : class App extends Component {

  // const { isLoading } = useAuth0();

  // if (isLoading) {
  //   return <Loading />;
  // }

  return (
    <div >
      <Header />
      <Main />
    </div>
  );
}


export default App;
